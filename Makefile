# Serves as a convenience driver for the CMAKE build system

mkfile_path     := $(abspath $(lastword $(MAKEFILE_LIST)))
mkfile_dir      := $(dir $(mkfile_path))
CMAKE_FILES     := $(shell find $(mkfile_dir)cmake -type f -name '*.cmake')
BUILD_DIR       ?= build
BAF_BINARY_PATH ?= $(BUILD_DIR_HOST)/src/binary-analysis-framework
CMAKE_BUILD_TYPE?= release
BUILD_DIR_HOST  ?= $(BUILD_DIR)/$(CMAKE_BUILD_TYPE)
CONFIGURE_TIMESTAMP_HOST ?= $(BUILD_DIR_HOST)/.timestamp

help:
	@echo "Targets:"
	@grep -oP "^[a-zA-Z-]+(?=[:])" "$(mkfile_path)"
.PHONY : help

clean:
	-rm -rf $(BUILD_DIR)
.PHONY : clean

# CMAKE uses a configure step to generate the build files. This target
# reconfigures if any of the CMAKE files or the Makefile change. Configuration
# does not need to occur for source code changes.
$(CONFIGURE_TIMESTAMP_HOST): CMakeLists.txt src/CMakeLists.txt $(CMAKE_FILES) $(mkfile_path)
	cmake -S "$(mkfile_dir)" -B "$(BUILD_DIR_HOST)" . -DCMAKE_BUILD_TYPE=$(CMAKE_BUILD_TYPE)
	@touch $(CONFIGURE_TIMESTAMP_HOST)

build: $(CONFIGURE_TIMESTAMP_HOST)
	cmake --build "$(BUILD_DIR_HOST)"

run: build
	$(BAF_BINARY_PATH)

test: build
	cd "$(BUILD_DIR_HOST)" && ctest -C $(CMAKE_BUILD_TYPE) --output-on-failure

coverage: build
	cd "$(BUILD_DIR_HOST)" && ctest -C coverage --verbose
