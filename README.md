# Binary Analysis Framework

This project enables investigating various binary formats at a detailed level.
It aims to provide an example of building GO binaries, complete with testing
and coverage, via CMAKE.

## Use

The application centers around the concept of _Byte Ranges_. A byte range
collects metadata about a contiguous sequence of bytes in an input binary. Byte
ranges are explicitly permitted to overlap; it allows increasing the
granularity of the information available. A `ByteRange` is a recursive
structure whose children may exceed the bounds of the parent. For example,
consider a `ByteRange` for a 64-bit OBJECT symbol entry at offset 0x4000; the
symbol itself resides at 0x6000 and is 4 bytes long. It will have a form
similar to :

```
symbol(0x4000, 24)
  |--Name   (0x4000, 4)
  |--Info   (0x4004, 1)
  |--Other  (0x4005, 1)
  |--Shndx  (0x4006, 2)
  |--Addr   (0x4008, 8)
  |--Size   (0x4010, 8)
  |--Data   (0x6000, 4)
```

The symbol entry is 24 bytes in length; each of the subfields comprising the
entry is additionally broken down. The location to which the symbol entry
points is stored in `Data`; note the offset is outside the entry itself. This
data structure decomposes each binary according to a logical instead of a
physical structure.

Due to the logical organization, any sequence of bytes is described by a set of
overlays of metadata that may not be proximal in the tree. In the above
example, `Data` is additionally described by the related `Section` entry into
which the offset falls (such as the `.data` section). To fully describe a byte
sequence, an application consuming the `ByteRange` must identify the set of
ranges that overlap the identified bytesr; the tree iteslf retains the
hierarchical nature of the data.

## Example Output

```
 build/release/src/binary-analysis-framework -verbose /bin/true
 {"level":"debug","ts":1648997978.4409688,"caller":"src/main.go:60","msg":"build-information","version":"c3178c0c4b629e1cd048c04ad6137f9c7a1b6f85","build-type":"Debug"}
{"level":"info","ts":1648997978.4413755,"caller":"src/elf.go:201","msg":"Decoding","type":"elf64"}
{"level":"debug","ts":1648997978.4414346,"caller":"src/elf_sections.go:299","msg":"shstrtab","index":29,"start":37044,"end":37329}
{"level":"debug","ts":1648997978.442166,"caller":"src/log.go:42","msg":"byte-range","name":"elf-64","description":"64-bit ELF file","index":0,"length":39256,"path":".elf-64"}
{"level":"debug","ts":1648997978.442194,"caller":"src/log.go:42","msg":"byte-range","name":"file-header","description":"File header for 64-bit ELF","index":0,"length":64,"path":".elf-64.file-header"}
{"level":"debug","ts":1648997978.4422112,"caller":"src/log.go:42","msg":"byte-range","name":"magic-bytes","description":"ELF Magic bytes","index":0,"length":4,"path":".elf-64.file-header.magic-bytes"}
.........SNIP........
{"level":"debug","ts":1648997978.4484682,"caller":"src/log.go:42","msg":"byte-range","name":"string-table-entry","description":".dynamic","index":37294,"length":8,"path":".elf-64.section-header-table..shstrtab..shstrtab-data.string-table-entry"}
{"level":"debug","ts":1648997978.448472,"caller":"src/log.go:42","msg":"byte-range","name":"string-table-entry","description":".data","index":37303,"length":5,"path":".elf-64.section-header-table..shstrtab..shstrtab-data.string-table-entry"}
{"level":"debug","ts":1648997978.448476,"caller":"src/log.go:42","msg":"byte-range","name":"string-table-entry","description":".bss","index":37309,"length":4,"path":".elf-64.section-header-table..shstrtab..shstrtab-data.string-table-entry"}
{"level":"debug","ts":1648997978.4484816,"caller":"src/log.go:42","msg":"byte-range","name":"string-table-entry","description":".gnu_debuglink","index":37314,"length":14,"path":".elf-64.section-header-table..shstrtab..shstrtab-data.string-table-entry"}
{"level":"debug","ts":1648997978.458324,"caller":"src/main.go:70","msg":"Range identified","length":39256}
```

The application only uses structured logging as output. The library [zap](https://github.com/uber-go/zap) serves as the logger. A [sample JSON](data/sample.json) is also available.

## Frontend

There is an under-development frontend for traversing the JSON file. It supports graphically viewing the structure of the parsed binary.

![Main Window](data/main-window.jpg)

# Building the Project

This project uses CMAKE and Go 1.17. A GNU-Makefile is provided as a
convenience driver for the CMAKE commands; `make help` will display a list of
commands available. To enable reproducibility, the GIT commit hash from HEAD is
provided (via
https://github.com/rpavlik/cmake-modules/blob/main/GetGitRevisionDescription.cmake)
as an environment variable. This variable can be built into each binary; see
[src/CMakeLists.txt](src/CMakeLists.txt) for an example.

# Testing

The `go` tooling imposes some project layout restrictions in regard to testing.
A convenience CMAKE function is provided (based on the work done at
https://github.com/mutse/go-cmake) to enable integrating the `go` testing
mechanism with CTEST. A coverage report is generated for each test under the
build directory.
