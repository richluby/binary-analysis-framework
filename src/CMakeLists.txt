include(golang)
include(git)

get_git_head_revision(GIT_REFSPEC GIT_SHA1)

# Build the CLI executable
add_go_executable(NAME binary-analysis-framework
    SOURCES
        main.go
        decode_binary.go
        decode_bytes.go
        binary_decoder.go
        cli.go
        config.go
        elf.go
        elf_dynamics.go
        elf_header.go
        elf_init_array.go
        elf_notes.go
        elf_note_types.go
        elf_program_bits.go
        elf_relocations.go
        elf_sections.go
        elf_section_text.go
        elf_strtab.go
        elf_symbols.go
        helpers_test.go

    # Common Sources
        byte_range.go
        error.go
        init_logger.go
        log.go
    TEST_SOURCES
        decode_binary_test.go
        binary_decoder_test.go
        cli_test.go
        elf_test.go
        error_test.go
    BUILD_OPTIONS
        -ldflags "-X main.Revision=${GIT_SHA1} -X main.BuildType=${CMAKE_BUILD_TYPE}"
    TEST_BUILD_OPTIONS
        -ldflags "-X baf/main.Revision=${GIT_SHA1} -X command-line-arguments.BuildType=${CMAKE_BUILD_TYPE}"
)

add_go_executable(NAME baf-gui
    SOURCES
        gui_layout.go
        gui_main.go

    # Common Sources
        byte_range.go
        error.go
        init_logger.go
        log.go
    TEST_SOURCES
    BUILD_OPTIONS
        -ldflags "-X main.Revision=${GIT_SHA1} -X main.BuildType=${CMAKE_BUILD_TYPE}"
    TEST_BUILD_OPTIONS

)
