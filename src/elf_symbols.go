package main

import (
	"debug/elf"
	"fmt"
	"unsafe"

	"go.uber.org/zap"
)

func decodeSymbolFields(theSectionHeader *elf.Section64,
	theSymbol *elf.Sym64,
	theIndex uint,
	theRange *ByteRange) {

	// The start of the symbol entry
	offset := uint(theSectionHeader.Off) + (uint(theSectionHeader.Entsize) * theIndex)

	name := ByteRange{
		Name:        "symbol-name",
		Description: "Offset in the linked strtab to the symbol name",
		Offset:      offset + uint(unsafe.Offsetof(theSymbol.Name)),
		Length:      uint(unsafe.Sizeof(theSymbol.Name)),
	}
	theRange.Children = append(theRange.Children, name)

	symbolType := elf.SymType(elf.ST_TYPE(theSymbol.Info)).String()
	infoType := ByteRange{
		Name:        "symbol-type",
		Description: "The type of the symbol: " + symbolType,
		Offset:      offset + uint(unsafe.Offsetof(theSymbol.Info)),
		Length:      uint(unsafe.Sizeof(theSymbol.Info)),
	}
	theRange.Children = append(theRange.Children, infoType)

	symbolBinding := elf.SymBind(elf.ST_BIND(theSymbol.Info)).String()
	infoBinding := ByteRange{
		Name:        "symbol-binding",
		Description: "The binding of the symbol: " + symbolBinding,
		Offset:      offset + uint(unsafe.Offsetof(theSymbol.Info)),
		Length:      uint(unsafe.Sizeof(theSymbol.Info)),
	}
	theRange.Children = append(theRange.Children, infoBinding)

	other := ByteRange{
		Name:        "symbol-other",
		Description: "Extra symbol information",
		Offset:      offset + uint(unsafe.Offsetof(theSymbol.Other)),
		Length:      uint(unsafe.Sizeof(theSymbol.Other)),
	}
	theRange.Children = append(theRange.Children, other)

	shndx := ByteRange{
		Name:        "symbol-shndx",
		Description: "Section index of the symbol",
		Offset:      offset + uint(unsafe.Offsetof(theSymbol.Shndx)),
		Length:      uint(unsafe.Sizeof(theSymbol.Shndx)),
	}
	theRange.Children = append(theRange.Children, shndx)

	value := ByteRange{
		Name:        "symbol-value",
		Description: "Value of the symbol",
		Offset:      offset + uint(unsafe.Offsetof(theSymbol.Value)),
		Length:      uint(unsafe.Sizeof(theSymbol.Value)),
	}
	theRange.Children = append(theRange.Children, value)

	size := ByteRange{
		Name:        "symbol-size",
		Description: "Size of the symbol",
		Offset:      offset + uint(unsafe.Offsetof(theSymbol.Size)),
		Length:      uint(unsafe.Sizeof(theSymbol.Size)),
	}
	theRange.Children = append(theRange.Children, size)

	data := ByteRange{
		Name:        "symbol-data",
		Description: "Data for the symbol",
		Offset:      uint(theSymbol.Value), // TODO VERIFY : Does this need (offset+value)
		Length:      uint(theSymbol.Size),
	}
	theRange.Children = append(theRange.Children, data)
}

func decodeSymtab(
	theElf *elfInformation,
	theSectionHeader *elf.Section64,
	theRange *ByteRange) BafError {

	// Get the linked section
	linkedSectionOffset := theElf.fileHeader.Shoff +
		(uint64(theElf.fileHeader.Shentsize) * uint64(theSectionHeader.Link))
	var linkedSection elf.Section64
	err := theElf.decoder.UnmarshalFromOffset(&linkedSection, uint(linkedSectionOffset))
	if err != nil {
		return NewBafErrorf(err.Code(), "Failed to unmarshal linked section : %d : %s",
			theSectionHeader.Link, err.Error())
	}
	names := theElf.fileBytes[linkedSection.Off : linkedSection.Off+linkedSection.Size]
	theElf.strtab = names

	endIndex := theSectionHeader.Size + theSectionHeader.Off
	var symbol elf.Sym64
	symbolSize := theSectionHeader.Entsize
	for offset := theSectionHeader.Off; offset < endIndex; offset += symbolSize {
		err = theElf.decoder.UnmarshalFromOffset(&symbol, uint(offset))
		if err != nil {
			return NewBafErrorf(err.Code(), "Decoder failed parsing symbol at offset : 0x%04x : %s", offset,
				err.Error())
		}
		theElf.symtab = append(theElf.symtab, symbol)
		symbolName := cStringFromOffset(names, uint(symbol.Name))
		log.Debug("add-symbol", zap.String(LogFieldDescription, fmt.Sprintf("%+v", symbol)),
			zap.String(LogFieldName, symbolName))

		symbolRange := ByteRange{
			Name:        "symbol-table-entry",
			Description: symbolName,
			Offset:      uint(offset),
			Length:      uint(theSectionHeader.Entsize),
		}
		decodeSymbolFields(theSectionHeader, &symbol, uint(offset), &symbolRange)
		theRange.Children = append(theRange.Children, symbolRange)
	}

	log.Debug("number-symbols", zap.Int(LogFieldLength, len(theElf.symtab)))
	return nil
}
