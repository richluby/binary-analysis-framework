package main

import (
	"debug/elf"
)

func decodeTextSection(theElf *elfInformation,
	theSectionHeader *elf.Section64,
	theRange *ByteRange,
	theSectionIndex uint) BafError {
	if theElf.symtab == nil {
		return NewBafErrorf(BafErrorInvalidArgument, "No symbol table identified")
	}

	for _, symbol := range theElf.symtab {
		symbolName := cStringFromOffset(theElf.strtab, uint(symbol.Name))
		if symbol.Shndx != uint16(theSectionIndex) {
			continue
		}

		symbolRange := ByteRange{
			Name:        "symbol-start",
			Description: "Start of symbol : " + symbolName,
			Offset:      uint(theSectionHeader.Off) + uint(symbol.Value),
			Length:      uint(symbol.Size),
		}
		theRange.Children = append(theRange.Children, symbolRange)
	}

	return nil
}
