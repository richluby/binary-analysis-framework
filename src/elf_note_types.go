package main

import "strconv"

// elfNoteHeader represents a header
// contained in the `.notes` section. The `debug/elf` ELF
// package does not contain a note header or comprehensive
// list of note types.
type elfNoteHeader struct {
	// NameSize is the size in bytes of the name
	NameSize uint32
	// DescriptionSize is the size in bytes of the description
	DescriptionSize uint32
	// Type the type of the note
	Type uint32
}

type elfNoteType uint32

// Provides constants for ELF notes
const (
	NTPrstatus       elfNoteType = 1
	NTPrfpreg        elfNoteType = 2
	NTFpregset       elfNoteType = 2
	NTPrpsinfo       elfNoteType = 3
	NTPrxreg         elfNoteType = 4
	NTTaskstruct     elfNoteType = 4
	NTPlatform       elfNoteType = 5
	NTAuxv           elfNoteType = 6
	NTGwindows       elfNoteType = 7
	NTAsrs           elfNoteType = 8
	NTPstatus        elfNoteType = 10
	NTPsinfo         elfNoteType = 13
	NTPrcred         elfNoteType = 14
	NTUtsname        elfNoteType = 15
	NTLwpstatus      elfNoteType = 16
	NTLwpsinfo       elfNoteType = 17
	NTPrfpxreg       elfNoteType = 20
	NTSiginfo        elfNoteType = 0x53494749
	NTFile           elfNoteType = 0x46494c45
	NTPrxfpreg       elfNoteType = 0x46e62b7f
	NTPpcVmx         elfNoteType = 0x100
	NTPpcSpe         elfNoteType = 0x101
	NTPpcVsx         elfNoteType = 0x102
	NTPpcTar         elfNoteType = 0x103
	NTPpcPpr         elfNoteType = 0x104
	NTPpcDscr        elfNoteType = 0x105
	NTPpcEbb         elfNoteType = 0x106
	NTPpcPmu         elfNoteType = 0x107
	NTPpcTmCgpr      elfNoteType = 0x108
	NTPpcTmCfpr      elfNoteType = 0x109
	NTPpcTmCvmx      elfNoteType = 0x10a
	NTPpcTmCvsx      elfNoteType = 0x10b
	NTPpcTmSpr       elfNoteType = 0x10c
	NTPpcTmCtar      elfNoteType = 0x10d
	NTPpcTmCppr      elfNoteType = 0x10e
	NTPpcTmCdscr     elfNoteType = 0x10f
	NTPpcPkey        elfNoteType = 0x110
	NT386Tls         elfNoteType = 0x200
	NT386Ioperm      elfNoteType = 0x201
	NTX86Xstate      elfNoteType = 0x202
	NTS390HighGprs   elfNoteType = 0x300
	NTS390Timer      elfNoteType = 0x301
	NTS390Todcmp     elfNoteType = 0x302
	NTS390Todpreg    elfNoteType = 0x303
	NTS390Ctrs       elfNoteType = 0x304
	NTS390Prefix     elfNoteType = 0x305
	NTS390LastBreak  elfNoteType = 0x306
	NTS390SystemCall elfNoteType = 0x307
	NTS390Tdb        elfNoteType = 0x308
	NTS390VxrsLow    elfNoteType = 0x309
	NTS390VxrsHigh   elfNoteType = 0x30a
	NTS390GsCb       elfNoteType = 0x30b
	NTS390GsBc       elfNoteType = 0x30c
	NTS390RiCb       elfNoteType = 0x30d
	NTArmVfp         elfNoteType = 0x400
	NTArmTLS         elfNoteType = 0x401
	NTArmHwBreak     elfNoteType = 0x402
	NTArmHwWatch     elfNoteType = 0x403
	NTArmSystemCall  elfNoteType = 0x404
	NTArmSve         elfNoteType = 0x405
	NTArmPacMask     elfNoteType = 0x406
	NTArmPacaKeys    elfNoteType = 0x407
	NTArmPacgKeys    elfNoteType = 0x408
	NTVmcoredd       elfNoteType = 0x700
	NTMipsDsp        elfNoteType = 0x800
	NTMipsFpMode     elfNoteType = 0x801
	NTMipsMsa        elfNoteType = 0x802
)

var noteTypeStrings = []intName{
	{1, "NT_PRSTATUS"},
	{2, "NT_PRFPREG"},
	{2, "NT_FPREGSET"},
	{3, "NT_PRPSINFO"},
	{4, "NT_PRXREG"},
	{4, "NT_TASKSTRUCT"},
	{5, "NT_PLATFORM"},
	{6, "NT_AUXV"},
	{7, "NT_GWINDOWS"},
	{8, "NT_ASRS"},
	{10, "NT_PSTATUS"},
	{13, "NT_PSINFO"},
	{14, "NT_PRCRED"},
	{15, "NT_UTSNAME"},
	{16, "NT_LWPSTATUS"},
	{17, "NT_LWPSINFO"},
	{20, "NT_PRFPXREG"},
	{0x53494749, "NT_SIGINFO"},
	{0x46494c45, "NT_FILE"},
	{0x46e62b7f, "NT_PRXFPREG"},
	{0x100, "NT_PPC_VMX"},
	{0x101, "NT_PPC_SPE"},
	{0x102, "NT_PPC_VSX"},
	{0x103, "NT_PPC_TAR"},
	{0x104, "NT_PPC_PPR"},
	{0x105, "NT_PPC_DSCR"},
	{0x106, "NT_PPC_EBB"},
	{0x107, "NT_PPC_PMU"},
	{0x108, "NT_PPC_TM_CGPR"},
	{0x109, "NT_PPC_TM_CFPR"},
	{0x10a, "NT_PPC_TM_CVMX"},
	{0x10b, "NT_PPC_TM_CVSX"},
	{0x10c, "NT_PPC_TM_SPR"},
	{0x10d, "NT_PPC_TM_CTAR"},
	{0x10e, "NT_PPC_TM_CPPR"},
	{0x10f, "NT_PPC_TM_CDSCR"},
	{0x110, "NT_PPC_PKEY"},
	{0x200, "NT_386_TLS"},
	{0x201, "NT_386_IOPERM"},
	{0x202, "NT_X86_XSTATE"},
	{0x300, "NT_S390_HIGH_GPRS"},
	{0x301, "NT_S390_TIMER"},
	{0x302, "NT_S390_TODCMP"},
	{0x303, "NT_S390_TODPREG"},
	{0x304, "NT_S390_CTRS"},
	{0x305, "NT_S390_PREFIX"},
	{0x306, "NT_S390_LAST_BREAK"},
	{0x307, "NT_S390_SYSTEM_CALL"},
	{0x308, "NT_S390_TDB"},
	{0x309, "NT_S390_VXRS_LOW"},
	{0x30a, "NT_S390_VXRS_HIGH"},
	{0x30b, "NT_S390_GS_CB"},
	{0x30c, "NT_S390_GS_BC"},
	{0x30d, "NT_S390_RI_CB"},
	{0x400, "NT_ARM_VFP"},
	{0x401, "NT_ARM_TLS"},
	{0x402, "NT_ARM_HW_BREAK"},
	{0x403, "NT_ARM_HW_WATCH"},
	{0x404, "NT_ARM_SYSTEM_CALL"},
	{0x405, "NT_ARM_SVE"},
	{0x406, "NT_ARM_PAC_MASK"},
	{0x407, "NT_ARM_PACA_KEYS"},
	{0x408, "NT_ARM_PACG_KEYS"},
	{0x700, "NT_VMCOREDD"},
	{0x800, "NT_MIPS_DSP"},
	{0x801, "NT_MIPS_FP_MODE"},
	{0x802, "NT_MIPS_MSA"},
}

// String converts an ElfNoteType to a string
func (i elfNoteType) String() string { return stringName(uint32(i), noteTypeStrings, false) }

// GoString converts an ElfNoteType to a string
func (i elfNoteType) GoString() string { return stringName(uint32(i), noteTypeStrings, true) }

type intName struct {
	i uint32
	s string
}

// stringName comes from `debug/elf`
func stringName(i uint32, names []intName, goSyntax bool) string {
	for _, n := range names {
		if n.i == i {
			if goSyntax {
				return "elf." + n.s
			}
			return n.s
		}
	}

	// second pass - look for smaller to add with.
	// assume sorted already
	for j := len(names) - 1; j >= 0; j-- {
		n := names[j]
		if n.i < i {
			s := n.s
			if goSyntax {
				s = "elf." + s
			}
			return s + "+" + strconv.FormatUint(uint64(i-n.i), 10)
		}
	}

	return strconv.FormatUint(uint64(i), 10)
}
