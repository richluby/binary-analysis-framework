// Collection of test helpers
// Some from https://github.com/benbjohnson/testing
package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

const testFile string = "/bin/true"
const readEntireFile = ^uint(0)
const sizeofHeader64 uint = 64

// assert fails the test if the condition is false.
func assert(tb testing.TB, condition bool, msg string, v ...interface{}) {
	if !condition {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d: "+msg+"\033[39m\n\n", append([]interface{}{filepath.Base(file), line}, v...)...)
		tb.FailNow()
	}
}

// ok fails the test if an err is not nil.
func ok(tb testing.TB, err error, theMessage string) {
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d: %s. Error: %s\033[39m\n\n", filepath.Base(file), line, theMessage, err.Error())
		tb.FailNow()
	}
}

// fail ensures that a failure case is properly identified
func fail(tb testing.TB, err BafError, theCode BafErrorCode, theMessage string) {
	if err == nil || err.Code() != theCode {
		_, file, line, _ := runtime.Caller(1)
		if err == nil {
			fmt.Printf("\033[31m%s:%d: %s. Error: %s\033[39m\n\n", filepath.Base(file), line, theMessage, "No error ID'ed")
		} else {
			fmt.Printf("\033[31m%s:%d: %s. Error: %s\033[39m\n\n", filepath.Base(file), line, theMessage, err.Error())
		}
		tb.FailNow()
	}
}

// equals fails the test if exp is not equal to act.
func equals(tb testing.TB, exp, act interface{}, theMessage string) {
	if !reflect.DeepEqual(exp, act) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\t%s: exp: %#v\tgot: %#v\033[39m\n", filepath.Base(file), line, theMessage, exp, act)
		tb.FailNow()
	}
}

// _readTestFile reads the requested number of bytes from the test file.
// 0 means readAll
func helperReadTestFile(t *testing.T, theBytes uint) []uint8 {
	file, err := os.Open(testFile)
	ok(t, err, "Failed to open test file")
	defer file.Close()

	if theBytes != readEntireFile {
		result := make([]uint8, theBytes)
		byteCount, err := file.Read(result)
		ok(t, err, "Failed to read from file")
		assert(t, byteCount == int(theBytes), "Failed to read full byte request : %d != %d",
			theBytes, byteCount)

		return result
	}

	result, err := ioutil.ReadAll(file)
	ok(t, err, "Failed to read all bytes from file")
	return result
}
