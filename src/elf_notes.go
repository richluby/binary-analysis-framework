package main

import "debug/elf"

// buildNote Builds the note range for a specified note
func buildNote(
	theElf *elfInformation,
	theNote *elfNoteHeader,
	theOffset uint) ByteRange {

	sizeofFields := uint(4)
	noteHeaderSize := uint(3 * sizeofFields)
	nameOffset := uint(theOffset) + (3 * sizeofFields)
	descriptionOffset := uint(theOffset) + (3 * sizeofFields) + uint(theNote.NameSize)
	headerRange := ByteRange{
		Name:        "note-entry",
		Description: "ELF Note",
		Offset:      theOffset,
		Length:      uint(noteHeaderSize) + uint(theNote.DescriptionSize) + uint(theNote.NameSize),
		Children: []ByteRange{
			{
				Name:        "note-name-size",
				Description: "Size of the name",
				Offset:      uint(theOffset),
				Length:      sizeofFields,
			},
			{
				Name:        "note-description-size",
				Description: "Size of the description",
				Offset:      theOffset + sizeofFields,
				Length:      sizeofFields,
			},
			{
				Name:        "note-type",
				Description: "Type : " + elf.NType(theNote.Type).GoString(),
				Offset:      theOffset + (2 * sizeofFields),
				Length:      sizeofFields,
			},
			{
				Name:        "note-name",
				Description: "Name : " + cStringFromOffset(theElf.fileBytes, nameOffset),
				Offset:      nameOffset,
				Length:      uint(theNote.NameSize),
			},
			{
				Name:        "note-description",
				Description: "Description of the note",
				Offset:      descriptionOffset,
				Length:      uint(theNote.DescriptionSize),
			},
		},
	}
	return headerRange
}

func decodeNotes(theElf *elfInformation,
	theSectionHeader *elf.Section64,
	theRange *ByteRange) BafError {
	var note elfNoteHeader
	endOffset := theSectionHeader.Off + theSectionHeader.Size
	sizeofFields := uint(4)
	noteHeaderSize := uint(3 * sizeofFields)

	for i := theSectionHeader.Off; i < endOffset; i += uint64(noteHeaderSize) {
		err := theElf.decoder.UnmarshalFromOffset(&note, uint(i))
		if err != nil {
			return NewBafErrorf(err.Code(), "Error decoding note at offset : %d - %s",
				i, err.Error())
		}

		headerRange := buildNote(theElf, &note, uint(i))
		theRange.Children = append(theRange.Children, headerRange)
		i += uint64(note.DescriptionSize) + uint64(note.NameSize)
	}

	return nil
}
