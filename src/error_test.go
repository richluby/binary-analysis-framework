package main

import (
	"fmt"
	"testing"
)

func TestErrorStringSet(t *testing.T) {
	equals(t, int(BafErrorUnknown)+1, len(bafErrorStrings),
		"Not all error codes have a related error message")
}

func TestErrorToString(t *testing.T) {
	err := NewBafErrorf(BafErrorBounds, "err-msg")
	equals(t, "Value out of bounds : err-msg", err.Error(),
		"Error failed to generate a string value")
}

func TestErrorOutOfBounds(t *testing.T) {
	errValue := BafErrorUnknown + 1
	equals(t,
		"Unknown error code : code out of bounds : "+fmt.Sprintf("%d", errValue),
		BafErrorCode(errValue).String(), "Failed to catch out of bounds error")
}

func TestErrorConversion(t *testing.T) {
	err := NewBafErrorf(BafErrorBounds, "err-msg")
	equals(t, uint(BafErrorBounds), err.Ucode(), "Failed to convert to UCODE correctly")
}
