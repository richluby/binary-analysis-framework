package main

import "go.uber.org/zap"

// DecodeBytes searches for MAGIC and decodes the array into a range of bytes
func DecodeBytes(theBytes []byte) (*ByteRange, BafError) {
	if log == nil {
		initLogger(false)
	}

	byteRange, bafErr := ParseElf(theBytes)
	if bafErr != nil {
		log.Debug("File not identified as ELF",
			zap.Uint(LogFieldErrorCode, bafErr.Ucode()),
			zap.String(LogFieldErrorMessage, bafErr.Error()))
		byteRange = nil
	}

	if byteRange == nil {
		return nil, NewBafErrorf(BafErrorNotImplemented, "No decoder implemented for associated bytes")
	}

	return byteRange, nil
}
