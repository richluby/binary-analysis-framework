package main

// ByteRange annotates the ranges within a binary
type ByteRange struct {
	// Offset describes the offset from the beginning
	Offset uint `json:"offset"`

	// Length describes the number of bytes present
	Length uint `json:"length"`

	// Description provides a human-friendly description
	// as a set of (fieldName, description) fields
	Description string `json:"description"`

	// Name holds the short name of this range
	Name string `json:"name"`

	// Children describes any child byte ranges that may comprise this range
	Children []ByteRange `json:"children,omitempty"`
}
