package main

import "debug/elf"

func decodeInitArray(theElf *elfInformation,
	theSectionHeader *elf.Section64,
	theRange *ByteRange) {

	stepSize := theSectionHeader.Entsize
	endIndex := theSectionHeader.Off + theSectionHeader.Size

	for i := theSectionHeader.Off; i < endIndex; i += stepSize {
		targetRange := ByteRange{
			Name:        "array-entry",
			Description: "Entry run by the dynamic loader",
			Offset:      uint(i),
			Length:      uint(theSectionHeader.Entsize),
		}
		theRange.Children = append(theRange.Children, targetRange)
	}
}
