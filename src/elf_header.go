package main

import (
	"debug/elf"
	"unsafe"
)

func decodeHeader64(theBytes []uint8, theHeader *elf.Header64) *ByteRange {
	var elfHeader ByteRange

	elfHeader.Description = "File header for 64-bit ELF"
	elfHeader.Length = uint(unsafe.Sizeof(*theHeader))
	elfHeader.Offset = 0
	elfHeader.Name = "file-header"

	magic := ByteRange{
		Name:        "magic-bytes",
		Description: "ELF Magic bytes",
		Length:      uint(len(elf.ELFMAG)),
		Offset:      0}
	elfHeader.Children = append(elfHeader.Children, magic)

	class := ByteRange{
		Name:        "elf-class",
		Description: "Architecure size of the ELF",
		Offset:      elf.EI_CLASS,
		Length:      1}
	elfHeader.Children = append(elfHeader.Children, class)

	data := ByteRange{
		Name:        "elf-data",
		Description: "Data order (MSB/LSB) of the binary data",
		Offset:      elf.EI_DATA,
		Length:      1}
	elfHeader.Children = append(elfHeader.Children, data)

	version := ByteRange{
		Name:        "elf-object-version",
		Description: "ELF Version",
		Offset:      elf.EI_VERSION,
		Length:      1}
	elfHeader.Children = append(elfHeader.Children, version)

	osabi := ByteRange{
		Name:        "elf-osabi",
		Description: "ELF OS Application Binary Inteface (ABI)",
		Offset:      elf.EI_OSABI,
		Length:      1}
	elfHeader.Children = append(elfHeader.Children, osabi)

	abiVersion := ByteRange{
		Name:        "elf-abi-version",
		Description: "ELF Application Binary Inteface (ABI) version",
		Offset:      elf.EI_ABIVERSION,
		Length:      1}
	elfHeader.Children = append(elfHeader.Children, abiVersion)

	eType := ByteRange{
		Name:        "elf-file-type",
		Description: "The type of ELF file (CORE/REL/SO/UNK)",
		Offset:      uint(unsafe.Offsetof(theHeader.Type)),
		Length:      uint(unsafe.Sizeof(theHeader.Type))}
	elfHeader.Children = append(elfHeader.Children, eType)

	eMachine := ByteRange{
		Name:        "elf-machine",
		Description: "The machine for the ELF file",
		Offset:      uint(unsafe.Offsetof(theHeader.Machine)),
		Length:      uint(unsafe.Sizeof(theHeader.Machine))}
	elfHeader.Children = append(elfHeader.Children, eMachine)

	eVersion := ByteRange{
		Name:        "elf-version",
		Description: "The verion for the ELF file",
		Offset:      uint(unsafe.Offsetof(theHeader.Version)),
		Length:      uint(unsafe.Sizeof(theHeader.Version))}
	elfHeader.Children = append(elfHeader.Children, eVersion)

	entry := ByteRange{
		Name:        "elf-entry",
		Description: "The entry point (_start) for the ELF file",
		Offset:      uint(unsafe.Offsetof(theHeader.Entry)),
		Length:      uint(unsafe.Sizeof(theHeader.Entry))}
	elfHeader.Children = append(elfHeader.Children, entry)

	phoff := ByteRange{
		Name:        "elf-phoff",
		Description: "The program header table offset for the ELF file",
		Offset:      uint(unsafe.Offsetof(theHeader.Phoff)),
		Length:      uint(unsafe.Sizeof(theHeader.Phoff))}
	elfHeader.Children = append(elfHeader.Children, phoff)

	shoff := ByteRange{
		Name:        "elf-shoff",
		Description: "The section header table offset for the ELF file",
		Offset:      uint(unsafe.Offsetof(theHeader.Shoff)),
		Length:      uint(unsafe.Sizeof(theHeader.Shoff))}
	elfHeader.Children = append(elfHeader.Children, shoff)

	flags := ByteRange{
		Name:        "elf-processor-flags",
		Description: "The processor flags for the ELF file",
		Offset:      uint(unsafe.Offsetof(theHeader.Flags)),
		Length:      uint(unsafe.Sizeof(theHeader.Flags))}
	elfHeader.Children = append(elfHeader.Children, flags)

	headerSize := ByteRange{
		Name:        "elf-header-size",
		Description: "The size of the header for the ELF file",
		Offset:      uint(unsafe.Offsetof(theHeader.Ehsize)),
		Length:      uint(unsafe.Sizeof(theHeader.Ehsize))}
	elfHeader.Children = append(elfHeader.Children, headerSize)

	phentsize := ByteRange{
		Name:        "elf-program-header-entry-size",
		Description: "The size of a program header for the ELF file",
		Offset:      uint(unsafe.Offsetof(theHeader.Phentsize)),
		Length:      uint(unsafe.Sizeof(theHeader.Phentsize))}
	elfHeader.Children = append(elfHeader.Children, phentsize)

	phnum := ByteRange{
		Name:        "elf-number-program-headers",
		Description: "The number of program headers for the ELF file",
		Offset:      uint(unsafe.Offsetof(theHeader.Phnum)),
		Length:      uint(unsafe.Sizeof(theHeader.Phnum))}
	elfHeader.Children = append(elfHeader.Children, phnum)

	shentsize := ByteRange{
		Name:        "elf-section-header-entry-size",
		Description: "The sizeof a section header for the ELF file",
		Offset:      uint(unsafe.Offsetof(theHeader.Shentsize)),
		Length:      uint(unsafe.Sizeof(theHeader.Shentsize))}
	elfHeader.Children = append(elfHeader.Children, shentsize)

	shnum := ByteRange{
		Name:        "elf-number-section-headers",
		Description: "The number of section headers for the ELF file",
		Offset:      uint(unsafe.Offsetof(theHeader.Shnum)),
		Length:      uint(unsafe.Sizeof(theHeader.Shnum))}
	elfHeader.Children = append(elfHeader.Children, shnum)

	shstrndx := ByteRange{
		Name:        "elf-shstrndx",
		Description: "The index of the section header string table for the ELF file",
		Offset:      uint(unsafe.Offsetof(theHeader.Shstrndx)),
		Length:      uint(unsafe.Sizeof(theHeader.Shstrndx))}
	elfHeader.Children = append(elfHeader.Children, shstrndx)

	return &elfHeader
}
