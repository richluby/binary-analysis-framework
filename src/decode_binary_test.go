package main

import (
	"os"
	"testing"
)

const testBinaryPath = "/bin/true"

func TestDecodeBinary(t *testing.T) {
	_, err := DecodeBinary(testBinaryPath)
	ok(t, err, "Error encountered while decoding the test binary")
}

func TestFullExecution(t *testing.T) {
	var config Configuration
	var err error
	config.InputPath = testBinaryPath
	config.outputFile, err = os.OpenFile(os.DevNull, os.O_WRONLY, os.ModeAppend)
	ok(t, err, "Failed to open /dev/null")
	err = executeConfig(&config)
	ok(t, err, "Error encountered while decoding the test binary")
}

func TestDecodeBinaryNotFound(t *testing.T) {
	_, err := DecodeBinary("non-existent-binary-path")
	fail(t, err, BafErrorReaderFailed,
		"Error encountered while decoding the test binary")
}

func TestDecodeBadBinary(t *testing.T) {
	header := helperReadTestFile(t, sizeofHeader64)
	header[0] = 0
	header[1] = 0
	_, err := DecodeBytes(header)
	fail(t, err, BafErrorNotImplemented,
		"Failed to recognize bad bytes")
}
