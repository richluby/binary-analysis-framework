package main

import (
	"encoding/binary"
	"io"
	"reflect"
)

type bafDecoderStruct struct {
	reader      io.ReadSeeker
	dataOrder   binary.ByteOrder
	cachedError BafError
}

// BafDecoder enables decoding bytes from a reader
type BafDecoder interface {
	Uint8() (uint8, BafError)
	Uint16() (uint16, BafError)
	Uint32() (uint32, BafError)
	Uint64() (uint64, BafError)
	Unmarshal(theStruct interface{}) BafError
	UnmarshalFromOffset(theStruct interface{}, theOffset uint) BafError
	Error() BafError
}

func (d *bafDecoderStruct) Uint8() (uint8, BafError) {
	var result uint8
	err := binary.Read(d.reader, d.dataOrder, &result)
	if err != nil {
		return 0, NewBafErrorf(BafErrorReaderFailed, "Failed while reading uint8 : %s", err.Error())
	}
	return result, nil
}
func (d *bafDecoderStruct) Uint16() (uint16, BafError) {
	var result uint16
	err := binary.Read(d.reader, d.dataOrder, &result)
	if err != nil {
		return 0, NewBafErrorf(BafErrorReaderFailed, "Failed while reading uint16 : %s", err.Error())
	}
	return result, nil
}
func (d *bafDecoderStruct) Uint32() (uint32, BafError) {
	var result uint32
	err := binary.Read(d.reader, d.dataOrder, &result)
	if err != nil {
		return 0, NewBafErrorf(BafErrorReaderFailed, "Failed while reading uint32 : %s", err.Error())
	}
	return result, nil
}
func (d *bafDecoderStruct) Uint64() (uint64, BafError) {
	var result uint64
	err := binary.Read(d.reader, d.dataOrder, &result)
	if err != nil {
		return 0, NewBafErrorf(BafErrorReaderFailed, "Failed while reading uint64 : %s", err.Error())
	}
	return result, nil
}
func (d *bafDecoderStruct) Int64() (int64, BafError) {
	var result int64
	err := binary.Read(d.reader, d.dataOrder, &result)
	if err != nil {
		return 0, NewBafErrorf(BafErrorReaderFailed, "Failed while reading int64 : %s", err.Error())
	}
	return result, nil
}

func (d *bafDecoderStruct) decodeArray(theField reflect.Value) BafError {
	var err BafError
	length := theField.Cap()

	// Iterate over each element in the array, setting its value appropriately
	for i := 0; i < length; i++ {
		element := theField.Index(i)
		d.decodeType(element)
	}
	return err
}

func (d *bafDecoderStruct) decodeType(theField reflect.Value) BafError {
	if d.cachedError != nil {
		return d.cachedError
	}
	switch theField.Kind() {
	case reflect.Array:
		return d.decodeArray(theField)
	case reflect.Uint8:
		var val uint8
		val, d.cachedError = d.Uint8()
		theField.SetUint(uint64(val))
	case reflect.Uint16:
		var val uint16
		val, d.cachedError = d.Uint16()
		theField.SetUint(uint64(val))
	case reflect.Uint32:
		var val uint32
		val, d.cachedError = d.Uint32()
		theField.SetUint(uint64(val))
	case reflect.Uint64:
		var val uint64
		val, d.cachedError = d.Uint64()
		theField.SetUint(val)
	case reflect.Int64:
		var val int64
		val, d.cachedError = d.Int64()
		theField.SetInt(val)
	case reflect.Uint:
		var val uint32
		val, d.cachedError = d.Uint32()
		theField.SetUint(uint64(val))
	case reflect.Int:
		var val uint32
		val, d.cachedError = d.Uint32()
		theField.SetInt(int64(val))
	default:
		d.cachedError = NewBafErrorf(BafErrorNotImplemented, "No decoder routine for : %s", theField.String())
	}
	return d.cachedError
}

func (d *bafDecoderStruct) UnmarshalFromOffset(theStruct interface{}, theOffset uint) BafError {
	if d.cachedError != nil {
		return d.cachedError
	}

	ptrType := reflect.ValueOf(theStruct)
	if ptrType.Kind() != reflect.Ptr {
		d.cachedError = NewBafErrorf(BafErrorInvalidArgument, "Non-pointer passed to unmarshal : %+v",
			ptrType.Kind())
		return d.cachedError
	}

	structType := ptrType.Elem()
	structKind := structType.Kind()
	if structKind != reflect.Struct {
		d.cachedError = NewBafErrorf(BafErrorInvalidArgument, "Pointer to non-struct value : %+v",
			structKind)
		return d.cachedError
	}

	_, err := d.reader.Seek(int64(theOffset), io.SeekStart)
	if err != nil {
		d.cachedError = NewBafErrorf(BafErrorReaderFailed, "Reader failed while seeking [%d] : %+v",
			theOffset, err)
		return d.cachedError
	}

	numberFields := structType.NumField()
	for i := 0; i < numberFields; i++ {
		fieldType := structType.Field(i)
		// fmt.Fprintf(os.Stderr, "Investigate field : %+v %s\n", fieldType.Kind(), fieldType.String())
		err := d.decodeType(fieldType)
		if err != nil {
			d.cachedError = err
			return d.cachedError
		}
	}

	return nil
}

func (d *bafDecoderStruct) Unmarshal(theStruct interface{}) BafError {
	currentOffset, _ := d.reader.Seek(0, io.SeekCurrent)
	return d.UnmarshalFromOffset(theStruct, uint(currentOffset))
}

func (d *bafDecoderStruct) Error() BafError {
	return d.cachedError
}

func newBAFDecoder(theReader io.ReadSeeker, theDataOrder binary.ByteOrder) BafDecoder {
	return &bafDecoderStruct{
		reader:      theReader,
		dataOrder:   theDataOrder,
		cachedError: nil,
	}
}
