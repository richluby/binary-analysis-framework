package main

import (
	"bytes"
	"encoding/binary"
	"testing"
)

type StructForTest struct {
	U8  uint8
	U16 uint16
	U32 uint32
	U64 uint64
	I   int
	UI  uint
	A   [4]uint8
}

func TestBinaryDecoder(t *testing.T) {
	testBytes := []uint8{
		0x01,    // U8
		0x02, 0, // U16
		0x03, 0, 0, 0, // U32
		0x04, 0, 0, 0, 0, 0, 0, 0, // U64
		0x05, 0, 0, 0, // int
		0x06, 0, 0, 0, // uint
		1, 2, 3, 4} // array

	reader := bytes.NewReader(testBytes)
	decoder := newBAFDecoder(reader, binary.LittleEndian)

	var testStruct StructForTest
	decoder.Unmarshal(&testStruct)

	equals(t, uint8(1), testStruct.U8, "Failed to set correct test struct value 8")
	equals(t, uint16(2), testStruct.U16, "Failed to set correct test struct value 16")
	equals(t, uint32(3), testStruct.U32, "Failed to set correct test struct value 32")
	equals(t, uint64(4), testStruct.U64, "Failed to set correct test struct value 64")
	equals(t, int(5), testStruct.I, "Failed to set correct test struct value I")
	equals(t, uint(6), testStruct.UI, "Failed to set correct test struct value UI")

	for i := 0; i < 4; i++ {
		assert(t, uint8(i+1) == testStruct.A[i], "Failed to set index correctly : %d != %d",
			i+1, testStruct.A[i])
	}
}
