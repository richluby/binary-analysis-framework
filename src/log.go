package main

const (
	// LogFieldUnhandled is used to warn about unhandled, non-fatal cases
	LogFieldUnhandled = "unhandled"

	// LogFieldType is the structured logging field for types
	LogFieldType = "type"

	// LogFieldPath is the structured logging field for paths
	LogFieldPath = "path"

	// LogFieldName is the structured logging field for names
	LogFieldName = "name"

	// LogFieldDescription is the structured logging field for descriptions
	LogFieldDescription = "description"

	// LogFieldVersion is the structured logging field for versions
	LogFieldVersion = "version"

	// LogFieldErrorCode is the structured logging field for error codes
	LogFieldErrorCode = "error_code"

	// LogFieldErrorMessage is the structured logging field for error messages
	LogFieldErrorMessage = "error_message"

	// LogFieldLength is the structured logging field for lengths
	LogFieldLength = "length"

	// LogFieldIndex is the structured logging field for indices
	LogFieldIndex = "index"

	// LogFieldStart is the structured logging field for indices
	LogFieldStart = "start"

	// LogFieldEnd is the structured logging field for indices
	LogFieldEnd = "end"
)
