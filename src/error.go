package main

import "fmt"

// BafErrorCode Type-safe variant for communicating error codes
type BafErrorCode uint

const (
	// BafSuccess Provides a success value
	BafSuccess BafErrorCode = iota
	// BafErrorBounds Provides an out of bounds error code
	BafErrorBounds
	// BafErrorUnknownArgument Specifies that an unknown argument occured
	BafErrorUnknownArgument
	// BafErrorInvalidArgument Specifies that an invalid (malformed) argument occurred
	BafErrorInvalidArgument

	// BafErrorReaderFailed identifies when a reader has failed to execute
	BafErrorReaderFailed
	// BafErrorWriterFailed identifies when a writer has failed to execute
	BafErrorWriterFailed

	// BafErrorNotImplemented is used when a feature is not supported
	BafErrorNotImplemented

	// BafErrorInvalidBytes is used when invalid bytes are encountered
	BafErrorInvalidBytes

	// BafErrorDecoderFailed describes a decoding failure
	BafErrorDecoderFailed

	// BafErrorEncoderFailed describes an encoding failure
	BafErrorEncoderFailed

	// BafErrorUnknown serves as the end of IOTA marker
	BafErrorUnknown
)

var bafErrorStrings = []string{
	"success",                                 // Success
	"Value out of bounds",                     // Bounds
	"Uknown argument provided",                // UNK Arg
	"Invalid argument provided",               // Invalid Arg
	"Reader encountered an error",             // Reader Error
	"Writer encountered an error",             // Writer Error
	"Not implemented",                         // Not implemented
	"Invalid byte sequence encountered",       // InvalidBytes
	"Decoder experienced an internal failure", // Decoder failed
	"Encoder experienced an internal failure", // Encoder failed
	"Unknown error occurred",
}

func (e BafErrorCode) String() string {
	if int(e) < len(bafErrorStrings) {
		return bafErrorStrings[e]
	}

	return fmt.Sprintf("Unknown error code : code out of bounds : %d", int(e))
}

// BafErrorType Provides a coded error message
type BafErrorType struct {
	code    BafErrorCode
	message string
}

// BafError enables a coded error message
type BafError interface {
	Code() BafErrorCode
	Ucode() uint
	Error() string
}

func (s *BafErrorType) Error() string {
	return fmt.Sprintf("%s : %s",
		s.code.String(),
		s.message)
}

// Code returns the code associated with this error
func (s *BafErrorType) Code() BafErrorCode {
	return s.code
}

// Ucode returns the uint code associated with this error
func (s *BafErrorType) Ucode() uint {
	return uint(s.code)
}

// NewBafErrorf Creates a coded error message
func NewBafErrorf(theCode BafErrorCode, theMessage string, args ...interface{}) BafError {
	return &BafErrorType{
		code:    theCode,
		message: fmt.Sprintf(theMessage, args...),
	}
}

// NewBafErrorFromErrorf Creates a coded error message from an existing one
func NewBafErrorFromErrorf(theError BafError, theMessage string, args ...interface{}) BafError {
	return &BafErrorType{
		code:    theError.Code(),
		message: fmt.Sprintf("%s : %s", fmt.Sprintf(theMessage, args...), theError.Error()),
	}
}
