package main

import (
	"fmt"
	"os"
)

// Configuration holds the context for te application
type Configuration struct {
	InputPath  string
	verbose    bool
	outputFile *os.File
}

func printUsage() {
	fmt.Fprintf(os.Stderr, "Usage : %s <path_to_input>\n", os.Args[0])
}

func initConfig() (*Configuration, error) {
	var config Configuration

	bafErr := parseArgs(&config, os.Args[1:])
	if bafErr != nil {
		return nil, bafErr
	}

	initLogger(config.verbose)

	return &config, nil
}
