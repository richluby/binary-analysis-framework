package main

import (
	"debug/elf"
	"unsafe"
)

func dynamicTypeToString(theTag int64) string {
	return elf.DynTag(theTag).GoString()
}

func decodeDynamics(theElf *elfInformation,
	theSectionHeader *elf.Section64,
	theRange *ByteRange) BafError {

	endIndex := theSectionHeader.Off + theSectionHeader.Size
	var dynamic elf.Dyn64
	stepSize := uint64(unsafe.Sizeof(dynamic))
	var counter uint = 0
	for i := theSectionHeader.Off; i < endIndex; i += stepSize {
		err := theElf.decoder.UnmarshalFromOffset(&dynamic, uint(i))
		if err != nil {
			return NewBafErrorf(err.Code(), "Failed to unmarshal dynamic[%d] - 0x%02x : %s",
				counter, i, err.Error())
		}

		tagSize := uint(unsafe.Sizeof(dynamic.Tag))
		dynamicRange := ByteRange{
			Name:        "dynamic-entry",
			Description: "DT entry",
			Offset:      uint(i),
			Length:      uint(theSectionHeader.Entsize),
			Children: []ByteRange{
				{
					Name:        "dynamic-entry-tag",
					Description: "DT entry of type : " + dynamicTypeToString(dynamic.Tag),
					Offset:      uint(i),
					Length:      tagSize,
				},
				{
					Name:        "dynamic-entry-value",
					Description: "Value of the dynamic entry",
					Offset:      uint(i + uint64(tagSize)),
					Length:      uint(unsafe.Sizeof(dynamic.Val)),
				},
			},
		}
		theRange.Children = append(theRange.Children, dynamicRange)

		counter++
	}

	return nil
}
