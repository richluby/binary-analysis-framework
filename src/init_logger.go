package main

import (
	"os"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func initLogger(isVerbose bool) {
	logConfig := zap.NewProductionEncoderConfig()
	encoder := zapcore.NewJSONEncoder(logConfig)
	logLevel := zapcore.InfoLevel
	if isVerbose {
		logLevel = zapcore.DebugLevel
	}
	core := zapcore.NewCore(encoder, zapcore.AddSync(os.Stderr), logLevel)
	log = zap.New(core, zap.AddCaller(), zap.AddStacktrace(zapcore.ErrorLevel))
	log.Debug("build-information", zap.String(LogFieldVersion, Revision),
		zap.String("build-type", BuildType))
}
