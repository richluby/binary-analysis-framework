package main

import (
	"testing"

	"go.uber.org/zap"
)

func init() {
	var err error
	log, err = zap.NewDevelopment(zap.AddStacktrace(zap.ErrorLevel))
	if err != nil {
		panic("Failed to init logging facility")
	}
}

func TestParseArgs(t *testing.T) {
	config := Configuration{}
	error := parseArgs(&config, []string{"-verbose", "/bin/true"})
	ok(t, error, "Failed to pass the test")
	equals(t, "/bin/true", config.InputPath, "ParseArgs failed to set input path correctly")
}

func TestParseArgsTooManyArgs(t *testing.T) {
	config := Configuration{}
	error := parseArgs(&config, []string{"/bin/true", "/bin/false"})
	fail(t, error, BafErrorBounds, "Failed to pass the test")
}

func TestParseArgsTooFewArgs(t *testing.T) {
	config := Configuration{}
	error := parseArgs(&config, []string{})
	fail(t, error, BafErrorBounds, "Failed to error for no arguments")
}

func TestParseArgsInvalidArgs(t *testing.T) {
	config := Configuration{}
	error := parseArgs(&config, []string{"-invalid-argument"})
	fail(t, error, BafErrorInvalidArgument, "Failed to error for invalid argument")
}
