package main

import (
	"debug/elf"

	"go.uber.org/zap"
)

func decodeProgramBits(theElf *elfInformation,
	theSectionHeader *elf.Section64,
	theName string,
	theRange *ByteRange,
	theSectionIndex uint) BafError {
	var err BafError

	switch theName {
	case ".interp":
	case ".init":
	case ".fini":
	case ".plt":
	case ".plt.got":
	case ".got":

	case ".rodata":
		fallthrough
	case ".data":
		fallthrough
	case ".text":
		err = decodeTextSection(theElf, theSectionHeader, theRange, theSectionIndex)
	default:
		log.Warn("Unknown section",
			zap.String(LogFieldName, theName))
	}

	return err
}
