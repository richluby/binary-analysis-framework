package main

import (
	"testing"

	"go.uber.org/zap"
)

func init() {
	var err error
	if BuildType == "Debug" || BuildType == "debug" {
		log, err = zap.NewDevelopment(zap.AddStacktrace(zap.ErrorLevel))
	} else {
		log, err = zap.NewProduction(zap.AddStacktrace(zap.ErrorLevel))
	}
	log.Debug("init-test", zap.String("build-type", BuildType))
	if err != nil {
		panic("Failed to init logging facility")
	}
}

func TestParseElfTooShort(t *testing.T) {
	testBytes := []uint8{}
	_, err := ParseElf(testBytes)
	fail(t, err, BafErrorBounds, "Attempted to continue with too few bytes in header")
}

func TestParseElfInvalidHeader(t *testing.T) {
	header := helperReadTestFile(t, sizeofHeader64)
	_, err := ParseElf(header)
	fail(t, err, BafErrorBounds, "Failed to mark file as too short")
}

func TestParseElfInvalidMagic(t *testing.T) {
	header := helperReadTestFile(t, sizeofHeader64)
	header[0] = 0
	header[1] = 0
	_, err := ParseElf(header)
	fail(t, err, BafErrorInvalidBytes, "Failed to recognize invalid magic bytes")
}

func verifyPresence(t *testing.T, theResult *ByteRange, theNames []string) {
	for _, check := range theNames {
		found := false
		for _, child := range theResult.Children {
			if child.Name == check {
				found = true
				break
			}
		}
		assert(t, found, "Failed to find name : %s", check)
	}
}

func findByName(theRange *ByteRange, theName string) *ByteRange {
	for _, child := range theRange.Children {
		if child.Name == theName {
			return &child
		}
	}

	for _, child := range theRange.Children {
		result := findByName(&child, theName)
		if result != nil {
			return result
		}
	}

	return nil
}

func TestDecodeElf64(t *testing.T) {
	testBytes := helperReadTestFile(t, readEntireFile)
	result, err := ParseElf(testBytes)
	ok(t, err, "Failed to parse the true bytes")

	assert(t, result.Description != "", "Failed to set description")
	assert(t, result.Name != "name", "Failed to set name")
	assert(t, result.Offset == 0, "Failed to set start value")
	assert(t, result.Length != 0, "Failed to set length")
	assert(t, result.Children != nil, "Failed to parse sub children")

	verifyPresence(t, result,
		[]string{"file-header", "section-header-table"})

	sections := findByName(result, "section-header-table")
	assert(t, sections != nil, "Failed to find the section header table")
	verifyPresence(t, sections,
		[]string{".text", ".dynsym", ".dynamic", ".shstrtab", ".dynstr"})

	textSection := findByName(result, ".text")
	assert(t, textSection != nil, "Failed to find the section header table")
	verifyPresence(t, textSection,
		[]string{"section-name-offset", "section-type", "section-flags",
			"section-virtual-address", "section-file-offset", "section-file-size",
			"section-link", "section-information", "section-alignment", "section-entry-size"})

	stringTableData := findByName(result, ".shstrtab-data")
	assert(t, stringTableData != nil, "Failed to find the string table data")
	verifyPresence(t, stringTableData,
		[]string{"string-table-entry"})

	symtabData := findByName(result, ".dynsym-data")
	assert(t, symtabData != nil, "Failed to find the string table data")
	verifyPresence(t, symtabData,
		[]string{"symbol-table-entry"})

	symbolTableEntry := findByName(result, "symbol-table-entry")
	assert(t, symbolTableEntry != nil, "Failed to find the symbol table data")
	verifyPresence(t, symbolTableEntry,
		[]string{"symbol-name", "symbol-type", "symbol-binding",
			"symbol-other", "symbol-shndx", "symbol-value",
			"symbol-size", "symbol-data",
		})

	relocationEntry := findByName(result, "relocation-entry")
	assert(t, relocationEntry != nil, "Failed to find any relocations")
	verifyPresence(t, relocationEntry,
		[]string{
			"relocation-offset", "relocation-info", "relocation-augmentation",
		})

	dynamicEntry := findByName(result, "dynamic-entry")
	assert(t, dynamicEntry != nil, "Failed to find any dynamic entries")
	verifyPresence(t, dynamicEntry,
		[]string{
			"dynamic-entry-tag", "dynamic-entry-value",
		})

	initArrayEntry := findByName(result, ".init_array-data")
	assert(t, initArrayEntry != nil, "Failed to find any init array entries")
	verifyPresence(t, initArrayEntry,
		[]string{
			"array-entry",
		})

	finiArrayEntry := findByName(result, ".fini_array-data")
	assert(t, finiArrayEntry != nil, "Failed to find any fini array entries")
	verifyPresence(t, finiArrayEntry,
		[]string{
			"array-entry",
		})

	noteEntry := findByName(result, "note-entry")
	assert(t, noteEntry != nil, "Failed to find any note entries")
	verifyPresence(t, noteEntry,
		[]string{
			"note-name-size", "note-description-size", "note-type",
			"note-name", "note-description",
		})

	symbolStartEntry := findByName(result, "symbol-start")
	assert(t, symbolStartEntry != nil, "Failed to find any symbol start entries")
}
