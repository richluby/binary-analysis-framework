package main

import (
	"debug/elf"
	"unsafe"

	"go.uber.org/zap"
)

type elfInformation struct {
	fileBytes  []uint8
	fileHeader *elf.Header64
	shstrtab   []uint8
	strtab     []uint8
	decoder    BafDecoder
	symtab     []elf.Sym64
}

func buildFieldsForSection(theSectionHeader *elf.Section64, theOwningRange *ByteRange) {
	name := ByteRange{
		Name:        "section-name-offset",
		Description: "Offset in the string table of the section",
		Offset:      uint(theSectionHeader.Off) + uint(unsafe.Offsetof(theSectionHeader.Name)),
		Length:      uint(unsafe.Sizeof(theSectionHeader.Name)),
	}
	theOwningRange.Children = append(theOwningRange.Children, name)

	sectionType := ByteRange{
		Name:        "section-type",
		Description: "Section with type : " + elf.SectionType(theSectionHeader.Type).GoString(),
		Offset:      uint(theSectionHeader.Off) + uint(unsafe.Offsetof(theSectionHeader.Type)),
		Length:      uint(unsafe.Sizeof(theSectionHeader.Type)),
	}
	theOwningRange.Children = append(theOwningRange.Children, sectionType)

	flags := ByteRange{
		Name:        "section-flags",
		Description: "Flags of the section",
		Offset:      uint(theSectionHeader.Off) + uint(unsafe.Offsetof(theSectionHeader.Flags)),
		Length:      uint(unsafe.Sizeof(theSectionHeader.Flags)),
	}
	theOwningRange.Children = append(theOwningRange.Children, flags)

	addr := ByteRange{
		Name:        "section-virtual-address",
		Description: "Virtual Address of the section",
		Offset:      uint(theSectionHeader.Off) + uint(unsafe.Offsetof(theSectionHeader.Addr)),
		Length:      uint(unsafe.Sizeof(theSectionHeader.Addr)),
	}
	theOwningRange.Children = append(theOwningRange.Children, addr)

	off := ByteRange{
		Name:        "section-file-offset",
		Description: "Offset of the section",
		Offset:      uint(theSectionHeader.Off) + uint(unsafe.Offsetof(theSectionHeader.Off)),
		Length:      uint(unsafe.Sizeof(theSectionHeader.Off)),
	}
	theOwningRange.Children = append(theOwningRange.Children, off)

	sectionSize := ByteRange{
		Name:        "section-file-size",
		Description: "Size of the section",
		Offset:      uint(theSectionHeader.Off) + uint(unsafe.Offsetof(theSectionHeader.Size)),
		Length:      uint(unsafe.Sizeof(theSectionHeader.Size)),
	}
	theOwningRange.Children = append(theOwningRange.Children, sectionSize)

	link := ByteRange{
		Name:        "section-link",
		Description: "Link information for the section",
		Offset:      uint(theSectionHeader.Off) + uint(unsafe.Offsetof(theSectionHeader.Link)),
		Length:      uint(unsafe.Sizeof(theSectionHeader.Link)),
	}
	theOwningRange.Children = append(theOwningRange.Children, link)

	info := ByteRange{
		Name:        "section-information",
		Description: "Additional information for the section",
		Offset:      uint(theSectionHeader.Off) + uint(unsafe.Offsetof(theSectionHeader.Info)),
		Length:      uint(unsafe.Sizeof(theSectionHeader.Info)),
	}
	theOwningRange.Children = append(theOwningRange.Children, info)

	align := ByteRange{
		Name:        "section-alignment",
		Description: "Alignment information for the section",
		Offset:      uint(theSectionHeader.Off) + uint(unsafe.Offsetof(theSectionHeader.Addralign)),
		Length:      uint(unsafe.Sizeof(theSectionHeader.Addralign)),
	}
	theOwningRange.Children = append(theOwningRange.Children, align)

	entSize := ByteRange{
		Name:        "section-entry-size",
		Description: "Size of entries within the section",
		Offset:      uint(theSectionHeader.Off) + uint(unsafe.Offsetof(theSectionHeader.Entsize)),
		Length:      uint(unsafe.Sizeof(theSectionHeader.Entsize)),
	}
	theOwningRange.Children = append(theOwningRange.Children, entSize)
}

func decodeSectionData(
	theElf *elfInformation,
	theSectionHeader *elf.Section64,
	theSectionIndex uint,
	theName string,
	theRange *ByteRange) BafError {

	var err BafError
	switch elf.SectionType(theSectionHeader.Type) {
	case elf.SHT_STRTAB:
		decodeStrtab(theSectionHeader, theName, theElf.fileBytes, theRange)
	case elf.SHT_DYNAMIC:
		err = decodeDynamics(theElf, theSectionHeader, theRange)
	case elf.SHT_DYNSYM:
		fallthrough
	case elf.SHT_SYMTAB:
		err = decodeSymtab(theElf, theSectionHeader, theRange)
	case elf.SHT_FINI_ARRAY:
		fallthrough
	case elf.SHT_INIT_ARRAY:
		decodeInitArray(theElf, theSectionHeader, theRange)
	case elf.SHT_NOTE:
		err = decodeNotes(theElf, theSectionHeader, theRange)
	case elf.SHT_GROUP:
	case elf.SHT_RELA:
		fallthrough
	case elf.SHT_REL:
		err = decodeRelocations(theElf, theSectionHeader, theRange)
	case elf.SHT_PROGBITS:
		err = decodeProgramBits(theElf, theSectionHeader, theName, theRange, theSectionIndex)
	case elf.SHT_GNU_HASH:
	case elf.SHT_NULL:
	case elf.SHT_NOBITS:
	case elf.SHT_GNU_VERSYM:
	case elf.SHT_GNU_VERNEED:
	default:
		log.Warn(LogFieldUnhandled,
			zap.String("Unhandled section type", elf.SectionType(theSectionHeader.Type).GoString()))
	}

	return err
}

func buildByteRangeForSection(theElf *elfInformation,
	theSectionHeader *elf.Section64,
	theSectionIndex uint) (*ByteRange, BafError) {
	// get the name
	if uint(theSectionHeader.Name) > uint(len(theElf.shstrtab)) {
		return nil, NewBafErrorf(BafErrorBounds, "Corrupted section header : name exceeds bytes : %d >%d",
			theSectionHeader.Name, len(theElf.shstrtab))
	}
	var byteRange ByteRange
	byteRange.Name = cStringFromOffset(theElf.shstrtab, uint(theSectionHeader.Name))
	byteRange.Description = "Section member in the ELF file"

	buildFieldsForSection(theSectionHeader, &byteRange)

	var dataRange ByteRange
	dataRange.Name = byteRange.Name + "-data"
	dataRange.Description = "Data for section " + byteRange.Name
	dataRange.Offset = uint(theSectionHeader.Off)
	dataRange.Length = uint(theSectionHeader.Size)

	err := decodeSectionData(theElf, theSectionHeader, theSectionIndex, byteRange.Name, &dataRange)
	if err != nil {
		return nil, NewBafErrorf(err.Code(), "Failed decoding section data : %s", err.Error())
	}

	byteRange.Children = append(byteRange.Children, dataRange)

	return &byteRange, nil
}

func decodeSections64(theElf *elfInformation) (*ByteRange, BafError) {
	// Find the names
	shstrtabHeaderOffset := uint(theElf.fileHeader.Shoff) + (uint(theElf.fileHeader.Shentsize) * uint(theElf.fileHeader.Shstrndx))
	var shstrtabHeader elf.Section64
	err := theElf.decoder.UnmarshalFromOffset(&shstrtabHeader, uint(shstrtabHeaderOffset))
	if err != nil {
		return nil, NewBafErrorf(err.Code(), "Error decoding shstrtab header: %s", err.Error())
	}
	log.Debug("shstrtab",
		zap.Uint(LogFieldIndex, uint(theElf.fileHeader.Shstrndx)),
		zap.Uint(LogFieldStart, uint(shstrtabHeader.Off)),
		zap.Uint(LogFieldEnd, uint(shstrtabHeader.Off+shstrtabHeader.Size)))
	theElf.shstrtab = theElf.fileBytes[shstrtabHeader.Off : shstrtabHeader.Off+shstrtabHeader.Size]

	var sectionHeaderTableEntry elf.Section64
	err = theElf.decoder.UnmarshalFromOffset(&sectionHeaderTableEntry, uint(theElf.fileHeader.Shoff))
	if err != nil {
		return nil, NewBafErrorf(BafErrorDecoderFailed, "Failed to decode section header table : %+v", err)
	}

	sectionResult := ByteRange{
		Name:        "section-header-table",
		Description: "The table containing information about all known sections",
		Offset:      uint(theElf.fileHeader.Shoff),
		Length:      (uint(theElf.fileHeader.Shnum * theElf.fileHeader.Shentsize)),
	}

	var sectionOffset uint
	i := uint(0)
	for ; i < uint(theElf.fileHeader.Shnum); i++ {
		// Insert the current section header
		sectionOffset = uint(theElf.fileHeader.Shoff) + uint(theElf.fileHeader.Shentsize)*i
		sectionRange, err := buildByteRangeForSection(theElf, &sectionHeaderTableEntry, i)
		if err != nil {
			return nil, NewBafErrorf(err.Code(), "Error decoding index %d : %s", i, err.Error())
		}
		sectionRange.Offset = uint(sectionOffset)
		sectionRange.Length = uint(theElf.fileHeader.Shentsize)
		sectionResult.Children = append(sectionResult.Children, *sectionRange)

		// Get the next section header
		err = theElf.decoder.UnmarshalFromOffset(&sectionHeaderTableEntry, uint(sectionOffset))
		if err != nil {
			return nil, NewBafErrorf(BafErrorDecoderFailed, "Failed to decode section header table : %+v", err)
		}
	}

	// Insert the final section header
	sectionRange, err := buildByteRangeForSection(theElf, &sectionHeaderTableEntry, i)
	if err != nil {
		return nil, NewBafErrorf(err.Code(), "Error decoding final section : %s", err.Error())
	}
	sectionRange.Offset = uint(sectionOffset)
	sectionRange.Length = uint(theElf.fileHeader.Shentsize)
	sectionResult.Children = append(sectionResult.Children, *sectionRange)

	return &sectionResult, nil
}
