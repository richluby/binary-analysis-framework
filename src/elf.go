package main

import (
	"bytes"
	"debug/elf"
	"encoding/binary"
	"unsafe"

	"go.uber.org/zap"
)

// ParseElf parses an ELF binary into the intermediate representation
func ParseElf(theBytes []uint8) (*ByteRange, BafError) {
	var header32 *elf.Header32
	smallHeaderSize := int(unsafe.Sizeof(*header32))
	if len(theBytes) < smallHeaderSize {
		return nil, NewBafErrorf(BafErrorBounds, "Not enough bytes for a program header: %d > %d",
			smallHeaderSize, len(theBytes))
	}

	if theBytes[0] != elf.ELFMAG[0] ||
		theBytes[1] != elf.ELFMAG[1] ||
		theBytes[2] != elf.ELFMAG[2] ||
		theBytes[3] != elf.ELFMAG[3] {
		return nil, NewBafErrorf(BafErrorInvalidBytes, "Invalid magic bytes : [0x%0x 0x%0x 0x%0x 0x%0x]",
			theBytes[0], theBytes[1], theBytes[2], theBytes[3])
	}

	dataOrder := theBytes[elf.EI_DATA]
	var endianness binary.ByteOrder = binary.LittleEndian
	if dataOrder == uint8(elf.ELFDATA2MSB) {
		endianness = binary.BigEndian
	}
	headerArch := theBytes[elf.EI_CLASS]

	if headerArch == uint8(elf.ELFCLASS64) {
		return decodeElf64(theBytes, endianness)
	}

	return decodeElf32(theBytes, endianness)
}

func cStringFromOffset(theBytes []uint8, theOffset uint) string {
	result := ""
	for i := theOffset; i < uint(len(theBytes)); i++ {
		if theBytes[i] == 0 {
			result = string(theBytes[theOffset:i])
			i = uint(len(theBytes))
		}
	}

	return result
}

func buildElfRange64(theElf *elfInformation) (*ByteRange, BafError) {
	var elfResult ByteRange
	log.Info("Decoding", zap.String(LogFieldType, "elf64"))

	elfResult.Description = "64-bit ELF file"
	elfResult.Length = uint(len(theElf.fileBytes))
	elfResult.Offset = 0
	elfResult.Name = "elf-64"

	header := decodeHeader64(theElf.fileBytes, theElf.fileHeader)
	elfResult.Children = append(elfResult.Children, *header)

	sections, err := decodeSections64(theElf)
	if err != nil {
		return nil, NewBafErrorf(err.Code(), "Failed to decode sections : %+v", err)
	}
	elfResult.Children = append(elfResult.Children, *sections)

	// logResultTree(&elfResult)
	return &elfResult, nil
}

func decodeElf64(theBytes []uint8, theByteOrder binary.ByteOrder) (*ByteRange, BafError) {
	byteReader := bytes.NewReader(theBytes)
	byteDecoder := newBAFDecoder(byteReader, theByteOrder)

	var fileHeader elf.Header64
	err := byteDecoder.Unmarshal(&fileHeader)
	if err != nil {
		return nil, NewBafErrorf(BafErrorReaderFailed, "Failed to decode file header : %s", err.Error())
	}

	byteCount := uint(len(theBytes))
	if uint64(byteCount) <= fileHeader.Shoff {
		return nil, NewBafErrorf(BafErrorBounds, "SHOFF is beyond end of file : %d >= %d",
			fileHeader.Shoff, byteCount)
	}
	if uint64(byteCount) <= fileHeader.Phoff {
		return nil, NewBafErrorf(BafErrorBounds, "PHOFF is beyond end of file : %d >= %d",
			fileHeader.Phoff, byteCount)
	}

	if fileHeader.Shnum == 0 {
		return nil, NewBafErrorf(BafErrorBounds, "sh_num is 0 : no sections in file")
	}

	elf := elfInformation{
		decoder:    byteDecoder,
		fileHeader: &fileHeader,
		fileBytes:  theBytes,
		symtab:     make([]elf.Sym64, 20),
		shstrtab:   nil,
		strtab:     nil,
	}
	return buildElfRange64(&elf)
}

func decodeElf32(theBytes []uint8, theByteOrder binary.ByteOrder) (*ByteRange, BafError) {
	log.Info("Decoding", zap.String(LogFieldType, "elf32"))
	return nil, NewBafErrorf(BafErrorNotImplemented, "decodeElf32 is not supported")
}
