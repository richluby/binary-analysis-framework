package main

import (
	"flag"
	"os"
)

// parseArgs parses the arguments into a usable configuration
//
// theConfiguration Pointer-receiver into which to place the arguments
//
// theArgs Collection of strings to be parsed. They should not include the command name.
func parseArgs(theConfiguration *Configuration,
	theArgs []string) BafError {
	if len(theArgs) < 1 {
		return NewBafErrorf(BafErrorBounds, "No arguments specified in command-line")
	}
	flagSet := flag.NewFlagSet("baf", flag.ContinueOnError)

	verbose := flagSet.Bool("verbose", false, "Enable verbose output")

	outputPath := flagSet.String("output", "STDOUT", "Path to which to write the output file")

	err := flagSet.Parse(theArgs)
	if err != nil {
		return NewBafErrorf(BafErrorInvalidArgument, err.Error())
	}

	if flagSet.NArg() != 1 {
		flagSet.Usage()
		return NewBafErrorf(BafErrorBounds,
			"Too many input binaries specified. 1 expected, received : %d",
			flagSet.NArg())
	}

	theConfiguration.InputPath = flagSet.Arg(0)
	theConfiguration.verbose = *verbose

	if *outputPath == "STDOUT" {
		theConfiguration.outputFile = os.Stdout
	} else {
		theConfiguration.outputFile, err = os.Open(*outputPath)
		if err != nil {
			return NewBafErrorf(BafErrorInvalidArgument, "Failed to open file [%s] : %s",
				*outputPath, err.Error())
		}
	}

	return nil
}
