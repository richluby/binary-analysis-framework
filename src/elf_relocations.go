package main

import (
	"debug/elf"
	"unsafe"
)

func decodeRelocations(
	theElf *elfInformation,
	theSectionHeader *elf.Section64,
	theRange *ByteRange) BafError {

	sectionOffset := theSectionHeader.Off
	end := sectionOffset + theSectionHeader.Size

	for i := sectionOffset; i < end; i += theSectionHeader.Entsize {
		var relocation elf.Rela64
		err := theElf.decoder.UnmarshalFromOffset(&relocation, uint(i))
		if err != nil {
			return NewBafErrorf(err.Code(), "Error parsing relocation : %d : %s", i, err.Error())
		}

		relEntryRange := ByteRange{
			Name:        "relocation-entry",
			Description: "Relocation for symbol",
			Offset:      uint(i),
			Length:      uint(theSectionHeader.Entsize),
		}

		off := ByteRange{
			Name:        "relocation-offset",
			Description: "Offset for relocation",
			Offset:      uint(i + uint64(unsafe.Offsetof(relocation.Off))),
			Length:      uint(unsafe.Sizeof(relocation.Off)),
		}
		relEntryRange.Children = append(relEntryRange.Children, off)

		info := ByteRange{
			Name:        "relocation-info",
			Description: "Information for relocation",
			Offset:      uint(i + uint64(unsafe.Offsetof(relocation.Info))),
			Length:      uint(unsafe.Sizeof(relocation.Info)),
		}
		relEntryRange.Children = append(relEntryRange.Children, info)

		augm := ByteRange{
			Name:        "relocation-augmentation",
			Description: "Augmentation for relocation",
			Offset:      uint(i + uint64(unsafe.Offsetof(relocation.Addend))),
			Length:      uint(unsafe.Sizeof(relocation.Addend)),
		}
		relEntryRange.Children = append(relEntryRange.Children, augm)

		theRange.Children = append(theRange.Children, relEntryRange)
	}

	return nil
}
