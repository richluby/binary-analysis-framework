package main

import (
	"io/ioutil"
	"os"
)

// DecodeBinary identifies the binary type via MAGIC and decodes it into a range of bytes
func DecodeBinary(thePath string) (*ByteRange, BafError) {
	if log == nil {
		initLogger(false)
	}
	file, err := os.Open(thePath)
	if err != nil {
		return nil, NewBafErrorf(BafErrorReaderFailed, "Failed to open path: [%s] %+v", thePath, err)
	}
	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, NewBafErrorf(BafErrorReaderFailed, "Failed to read file : [%s] %+v", thePath, err)
	}

	return DecodeBytes(bytes)
}
