package main

import (
	"encoding/json"
	"fmt"
	"os"

	"go.uber.org/zap"
)

var (
	// Revision enables reproducible builds by embedding revisions into the binary
	Revision string
	// BuildType enables logging choices determined by the type of build
	BuildType string
	log       *zap.Logger
	config    *Configuration
)

func executeConfig(theConfig *Configuration) error {
	byteRange, bafErr := DecodeBinary(theConfig.InputPath)
	if bafErr != nil {
		log.Error("Error occurred decoding",
			zap.Uint(LogFieldErrorCode, bafErr.Ucode()),
			zap.String(LogFieldErrorMessage, bafErr.Error()))
		return bafErr
	}
	log.Debug("Range identified", zap.Uint(LogFieldLength, byteRange.Length))

	jsonEncoder := json.NewEncoder(theConfig.outputFile)
	jsonEncoder.SetIndent("", "  ")
	err := jsonEncoder.Encode(byteRange)
	if err != nil {
		log.Error("Error occurred encoding JSON",
			zap.Uint(LogFieldErrorCode, uint(BafErrorEncoderFailed)),
			zap.String(LogFieldErrorMessage, err.Error()))
		return err
	}

	return nil
}

func main() {
	var err error
	config, err = initConfig()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error occurred during configuration : %+v\n", err)
		printUsage()
		os.Exit(int(BafErrorInvalidArgument))
	}
	defer log.Sync()

	err = executeConfig(config)
	if err != nil {
		os.Exit(1)
	}
}
