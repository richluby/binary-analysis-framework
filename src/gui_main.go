package main

import (
	"fmt"
	"os"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/widget"
	"go.uber.org/zap"
)

var (
	// Revision enables reproducible builds by embedding revisions into the binary
	Revision string
	// BuildType enables logging choices determined by the type of build
	BuildType string
	// log is the logger for the application
	log *zap.Logger
)

const (
	indexJSON     uint = 1
	indexFilePath uint = 2
)

func main() {
	if len(os.Args) < 3 {
		fmt.Fprintf(os.Stderr, "Usage : %s <json-path> <file-path>\n", os.Args[0])
		os.Exit(int(BafErrorInvalidArgument))
	}

	initLogger(true)
	defer log.Sync()

	json := os.Args[indexJSON]
	filePath := os.Args[indexFilePath]
	log.Debug("JSON:", zap.String(LogFieldPath, json))
	log.Debug("FILE:", zap.String(LogFieldPath, filePath))

	a := app.New()
	a.Settings().Theme().Size(fyne.ThemeSizeName("Small"))
	w := a.NewWindow("BAF Display")
	w.SetContent(widget.NewLabel("Loading..."))
	w.Show()
	err := layoutNewWindow(json, filePath, &w)
	if err != nil {
		log.Error("Application encountered fatal error",
			zap.String(LogFieldErrorMessage, err.Error()),
			zap.Uint(LogFieldErrorCode, err.Ucode()))
		os.Exit(int(err.Ucode()))
	}
	w.Content().Refresh()
	w.RequestFocus()
	a.Run()
}
