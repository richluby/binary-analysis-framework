package main

import "debug/elf"

func decodeStrtab(theSectionHeader *elf.Section64, theName string,
	theBytes []uint8,
	theRange *ByteRange) {

	endIndex := theSectionHeader.Size + theSectionHeader.Off

	stringStart := theSectionHeader.Off
	for i := theSectionHeader.Off; i < endIndex; i++ {
		if theBytes[i] == 0 {
			stringRange := ByteRange{
				Name:        "string-table-entry",
				Description: string(theBytes[stringStart:i]),
				Offset:      uint(stringStart),
				Length:      uint(i - stringStart),
			}

			theRange.Children = append(theRange.Children, stringRange)
			stringStart = i + 1
		}
	}
}
