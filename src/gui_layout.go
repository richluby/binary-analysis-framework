package main

import (
	"encoding/json"
	"fmt"
	"os"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
	"go.uber.org/zap"
)

func getJSONDecoder(theJSON string) (*ByteRange, BafError) {
	inputFile, err := os.ReadFile(theJSON)
	if err != nil {
		return nil, NewBafErrorf(BafErrorReaderFailed, "Failed to open JSON file : %+v",
			err)
	}

	var byteRange ByteRange
	err = json.Unmarshal(inputFile, &byteRange)
	if err != nil {
		return nil, NewBafErrorf(BafErrorDecoderFailed, "Failed to decode JSON : %+v", err)
	}

	return &byteRange, nil
}

func getFileBytes(theFilePath string) ([]byte, BafError) {
	inputFile, err := os.ReadFile(theFilePath)
	if err != nil {
		return nil, NewBafErrorf(BafErrorReaderFailed, "Failed to open file: %+v : [%s]",
			err, theFilePath)
	}

	return inputFile, nil
}

func addTable(theBytes []byte) *widget.Table {
	const maxColumns int = 32
	var byteCount int = len(theBytes)
	const placeholder string = "    "

	// Used to enable a closure over the bytes
	table := widget.NewTable(
		func() (int, int) {
			return byteCount / maxColumns, maxColumns
		},
		func() fyne.CanvasObject {
			label := widget.NewLabel(placeholder)
			label.TextStyle.Monospace = true
			return label
		},
		func(theCoordinate widget.TableCellID, theOwner fyne.CanvasObject) {
			offset := theCoordinate.Row*maxColumns + theCoordinate.Col
			if offset > byteCount {
				theOwner.(*widget.Label).SetText(placeholder)
			} else {
				theOwner.(*widget.Label).SetText(fmt.Sprintf("0x%02x", theBytes[offset]))
			}
		})
	table.OnSelected = func(theCell widget.TableCellID) {
		log.Debug("Select Cell", zap.Int("x", theCell.Row), zap.Int("y", theCell.Col))
	}

	return table
}

type byteRangeMap struct {
	byteRange   *ByteRange
	uidChildren []string
}

func buildTreeMap(theRanges *ByteRange,
	theMap *map[string]byteRangeMap,
	theCount int) (int, string) {

	current := byteRangeMap{byteRange: theRanges,
		uidChildren: make([]string, len(theRanges.Children))}
	uid := fmt.Sprintf("%04d", theCount)
	(*theMap)[uid] = current

	// Start at 1 for the current node
	childCount := 1
	for i := 0; i < len(theRanges.Children); i++ {
		count, childUID := buildTreeMap(&theRanges.Children[i], theMap, theCount+childCount)
		current.uidChildren[i] = childUID
		childCount += count
		log.Debug("make-uid", zap.String(LogFieldName, current.uidChildren[i]))
	}
	return childCount + len(theRanges.Children), uid
}

func getTree(theRanges *ByteRange) *widget.Tree {
	treeUIDMap := make(map[string]byteRangeMap)
	_, defaultUID := buildTreeMap(theRanges, &treeUIDMap, 1)

	tree := widget.NewTree(
		// Return tree child UIDs
		func(theUid widget.TreeNodeID) []widget.TreeNodeID {
			if "" == theUid {
				theUid = defaultUID
			}

			byteRangeObject := treeUIDMap[theUid]
			log.Debug("get-uid", zap.Int(LogFieldLength, len(byteRangeObject.uidChildren)),
				zap.String(LogFieldName, theUid))
			return byteRangeObject.uidChildren
		},

		// Return true if is a branch
		func(theUid widget.TreeNodeID) bool {
			if "" == theUid {
				theUid = defaultUID
			}

			log.Debug("uid-branch", zap.String(LogFieldPath, theUid))
			byteRange := treeUIDMap[theUid]
			return byteRange.byteRange != nil && byteRange.byteRange.Children != nil
		},

		// Cacheable template object????
		func(b bool) fyne.CanvasObject {
			return widget.NewLabel("Tree Node")
		},

		// Update the data at the cached object
		func(theUid widget.TreeNodeID, b bool, theLabel fyne.CanvasObject) {
			if "" == theUid {
				theUid = defaultUID
			}

			log.Debug("uid-draw", zap.String(LogFieldPath, theUid))
			labelText := fmt.Sprintf("0x%04x:%db %s",
				treeUIDMap[theUid].byteRange.Offset,
				treeUIDMap[theUid].byteRange.Length,
				treeUIDMap[theUid].byteRange.Name)
			theLabel.(*widget.Label).SetText(labelText)
		})

	tree.OnSelected = func(uid widget.TreeNodeID) {
		log.Debug("UID node selected", zap.String(LogFieldName, uid))
	}
	return tree
}

func layoutNewWindow(theJSON string,
	theFilePath string,
	theWindow *fyne.Window) BafError {

	byteRange, err := getJSONDecoder(theJSON)
	if err != nil {
		return NewBafErrorf(err.Code(), "Error occurred getting JSON decoder :%v", err)
	}

	fileBytes, err := getFileBytes(theFilePath)
	if err != nil {
		return NewBafErrorf(BafErrorReaderFailed, "Failed to read input file : %+v - [%s]",
			err, theFilePath)
	}

	log.Debug("File size", zap.Int(LogFieldLength, len(fileBytes)))
	log.Debug("Decoded Range", zap.String(LogFieldName, byteRange.Name),
		zap.String(LogFieldDescription, byteRange.Description))

	table := addTable(fileBytes)
	tree := getTree(byteRange)
	split := container.NewHSplit(table, tree)

	(*theWindow).SetContent(split)

	return nil
}
