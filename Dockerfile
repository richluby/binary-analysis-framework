FROM golang:latest


# Install additional OS packages.
RUN apt-get update && export DEBIAN_FRONTEND=noninteractive \
    && apt-get -y install --no-install-recommends \
        cmake \
        gcc \
        libgl1-mesa-dev \
        xorg-dev && \
    rm -r /var/lib/apt/lists /var/cache/apt/archives

# Use go get to install go deps
RUN go get -x golang.org/x/lint/golint && \
    go get fyne.io/fyne/v2
