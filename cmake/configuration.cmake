# Adapted from https://riptutorial.com/cmake/example/26702/setting-a-release-debug-configuration

include(policies)

set(CMAKE_CONFIGURATION_TYPES "Debug;Release;Coverage" CACHE STRING "Supported configuration types" FORCE)
if(DEFINED CMAKE_BUILD_TYPE AND CMAKE_VERSION VERSION_GREATER "2.8")
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS  ${CMAKE_CONFIGURATION_TYPES})
elseif(NOT DEFINED CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE "Release" CACHE STRING "Default built type selected")
endif()