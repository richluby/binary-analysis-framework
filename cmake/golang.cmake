# Adapted from https://github.com/mutse/go-cmake

set(GOPATH "${CMAKE_CURRENT_BINARY_DIR}/go")
file(MAKE_DIRECTORY ${GOPATH})

function(ExternalGoProject_Add NAME)
  add_custom_target(${NAME} env GOPATH=${GOPATH} ${CMAKE_Go_COMPILER} get ${ARGN})
endfunction(ExternalGoProject_Add)

# Enables adding a series of go tests
#
# NAME The name to use for the test
#
# BUILD_OPTIONS List of additional flags to pass to compiler
#
# SOURCES List of source files for the executable
#
# TEST_SOURCES List of sources for tests
#
# TEST_ARGS List of arguments passed to the test
#
# CONFIGURATION Test configuration in which to add this test
function(add_go_test)
  set(one_value_args NAME)
  set(multi_value_args CONFIGURATION BUILD_OPTIONS SOURCES TEST_SOURCES TEST_ARGS)
  cmake_parse_arguments(PARSED "" "${one_value_args}" "${multi_value_args}" ${ARGN})

  set(TEST_EXEC_PATH "${CMAKE_CURRENT_BINARY_DIR}/${PARSED_NAME}")

  add_custom_command(
    OUTPUT "${TEST_EXEC_PATH}"

    # Build test
    COMMAND env GOPATH=${GOPATH} ${CMAKE_Go_COMPILER} test
      -c -o "${TEST_EXEC_PATH}"
      ${PARSED_BUILD_OPTIONS}
      ${PARSED_TEST_SOURCES} ${PARSED_SOURCES}

    WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
    COMMENT "Building test : ${TEST_EXEC_PATH}"
    DEPENDS ${PARSED_SOURCES} ${PARSED_TEST_SOURCES}
  )

  add_custom_target("${PARSED_NAME}" ALL
    DEPENDS "${TEST_EXEC_PATH}"
  )

  set(TEST_CONFIGURATION ${CMAKE_BUILD_TYPE})
  if(DEFINED PARSED_CONFIGURATION)
    set(TEST_CONFIGURATION ${PARSED_CONFIGURATION})
  endif()

  # Runs the test and generates a coverage report for viewing
  add_test(NAME ${PARSED_NAME}
    COMMAND /usr/bin/env sh -c "${TEST_EXEC_PATH} ${PARSED_TEST_ARGS}"
    CONFIGURATIONS ${TEST_CONFIGURATION}
  )
  message(STATUS "Test ${PARSED_NAME} configuration : ${TEST_CONFIGURATION}")
endfunction()

# Enables adding a GO executable
#
# BUILD_OPTIONS List of additional flags to pass to compiler
#
# TEST_BUILD_OPTIONS List of additional flags to pass to test builder
#
# DEPENDENCIES List of (name, URL) tuple dependencies for `go get`
#
# SOURCES List of source files for the executable
#
# TEST_SOURCES List of sources for tests
function(add_go_executable)

  set(one_value_args NAME)
  set(multi_value_args BUILD_OPTIONS TEST_BUILD_OPTIONS SOURCES TEST_SOURCES DEPENDENCIES)
  cmake_parse_arguments(PARSED "" "${one_value_args}" "${multi_value_args}" ${ARGN})

  foreach(DEP ${PARSED_DEPENDENCIES})
    ExternalGoProject_Add(${DEP})
  endforeach()

  if(NOT PARSED_SOURCES)
    message(FATAL_ERROR "No sources provided for ${PARSED_NAME}")
  endif()

  ## Executable
  message(STATUS "Configuring Go-exe: ${PARSED_NAME}")
  set(OUTPUT_FILE_PATH "${CMAKE_CURRENT_BINARY_DIR}/${PARSED_NAME}")

  add_custom_command(OUTPUT "${OUTPUT_FILE_PATH}"
    WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
    COMMENT "Processing : ${OUTPUT_FILE_PATH}"
    DEPENDS ${PARSED_SOURCES}

    COMMAND env GOPATH=${GOPATH} ${CMAKE_Go_COMPILER} fmt
      ${PARSED_SOURCES} ${PARSED_TEST_SOURCES}
  )

  add_custom_command(OUTPUT "${OUTPUT_FILE_PATH}"
    APPEND
    WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
    COMMENT "Vetting : ${OUTPUT_FILE_PATH}"
    DEPENDS ${PARSED_SOURCES}

    COMMAND env GOPATH=${GOPATH} ${CMAKE_Go_COMPILER} vet
      ${PARSED_SOURCES} ${PARSED_TEST_SOURCES}
  )

  add_custom_command(OUTPUT "${OUTPUT_FILE_PATH}"
    APPEND
    WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
    COMMENT "Linting : ${OUTPUT_FILE_PATH}"
    DEPENDS ${PARSED_SOURCES}

    COMMAND env GOPATH=${GOPATH} golint
      -set_exit_status
      ${PARSED_SOURCES} ${PARSED_TEST_SOURCES}
  )

  add_custom_command(OUTPUT "${OUTPUT_FILE_PATH}"
    APPEND
    WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
    COMMENT "Building : ${OUTPUT_FILE_PATH}"
    DEPENDS ${PARSED_SOURCES}

    COMMAND env GOPATH=${GOPATH} ${CMAKE_Go_COMPILER} build
      -o "${OUTPUT_FILE_PATH}"
      ${CMAKE_GO_FLAGS} ${PARSED_BUILD_OPTIONS}
      ${PARSED_SOURCES}
  )

  add_custom_target(${PARSED_NAME} ALL DEPENDS "${OUTPUT_FILE_PATH}")
  install(PROGRAMS "${OUTPUT_FILE_PATH}" DESTINATION bin)

  if((BUILD_TESTING) AND (PARSED_TEST_SOURCES))
    foreach(test_source ${PARSED_TEST_SOURCES})
      string(REPLACE ".go" "" TEST_NAME ${test_source})
        add_go_test(NAME ${TEST_NAME}
          CONFIGURATION Debug Release
          SOURCES
            ${PARSED_SOURCES}
          TEST_SOURCES
            "${test_source}"
          BUILD_OPTIONS
            ${PARSED_TEST_BUILD_OPTIONS}
          TEST_ARGS
            # Additional arguments to test here
        )
      endforeach()

      set(TEST_COVERAGE "${OUTPUT_FILE_PATH}.coverage")
      add_go_test(NAME "${PARSED_NAME}-coverage"
        CONFIGURATION Coverage
        SOURCES ${PARSED_SOURCES}
        TEST_SOURCES ${PARSED_TEST_SOURCES}
        BUILD_OPTIONS
          -coverprofile="${TEST_COVERAGE}"
        TEST_ARGS
          "-test.coverprofile \"${TEST_COVERAGE}\" && go tool cover -html=${TEST_COVERAGE} -o ${TEST_COVERAGE}.html"
      )
  endif()
endfunction(add_go_executable)

function(ADD_GO_LIBRARY NAME BUILD_TYPE)
  if(BUILD_TYPE STREQUAL "STATIC")
    set(BUILD_MODE -buildmode=c-archive)
    set(LIB_NAME "lib${NAME}.a")
  else()
    set(BUILD_MODE -buildmode=c-shared)
    if(APPLE)
      set(LIB_NAME "lib${NAME}.dylib")
    else()
      set(LIB_NAME "lib${NAME}.so")
    endif()
  endif()

  file(GLOB GO_SOURCE RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" "*.go")
  add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/.timestamp
    COMMAND env GOPATH=${GOPATH} ${CMAKE_Go_COMPILER} build ${BUILD_MODE}
    -o "${CMAKE_CURRENT_BINARY_DIR}/${LIB_NAME}"
    ${CMAKE_GO_FLAGS} ${GO_SOURCE}
    WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR})

  add_custom_target(${NAME} ALL DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/.timestamp ${ARGN})

  if(NOT BUILD_TYPE STREQUAL "STATIC")
    install(PROGRAMS ${CMAKE_CURRENT_BINARY_DIR}/${LIB_NAME} DESTINATION bin)
  endif()
endfunction(ADD_GO_LIBRARY)
