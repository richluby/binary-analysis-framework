import "list"

// Size templates
_elf_addr: {
	length: _size.uint32
	options: active_arch_multiplier: true
}
_elf_off: _elf_addr & {}

_elf_skip_if_32: {
	options: skip_if: "elf_header.e_ident.ei_class == _elf_class.elf32"
}

_elf_skip_if_64: {
	options: skip_if: "elf_header.e_ident.ei_class == _elf_class.elf64"
}

// 0x7fELF
_elf_magic_bytes: [0x7f, 0x45, 0x4c, 0x46]
_elf_class: {
	none:  0
	elf32: 1
	elf64: 2
}
_elf_data: {
	none: 0
	lsb:  1
	msb:  2
}
_elf_osabi: {
	none:       0
	sysv:       1
	hpux:       2
	netbsd:     3
	linux:      4
	solaris:    5
	irix:       6
	freebsd:    7
	tru64:      8
	arm:        9
	standalone: 10
}

elf_header: #binary_component & {
	name:        "elf_header"
	description: "Header that identifies the layout of the file"
	generates: [
		_e_ident,
		_e_type,
		_e_machine,
		_e_version,
		_e_entry,
		_e_phoff,
		_e_shoff,
		_e_flags,
		_e_ehsize,
		_e_phentsize,
		_e_phnum,
		_e_shentsize,
		_e_shnum,
		_e_shstrndx,
	]
}

_e_ident: #binary_component & {
	description: "Values necessary for the parser to understand the binary"
	generates:   list.Concat([
			[ for key, _ in _elf_magic_bytes {_ei_magic["ei_mag[\(key)]"]}],
			[
				_ei_class,
				_ei_data,
				_ei_version,
				_ei_osabi,
				_ei_abiversion,
				_ei_pad,
				_ei_nident,
			],
	])
	name: "e_ident"
}

// ========================================
// E_IDENT Array Starts
// ========================================
_ei_magic: {
	for key, magic_value in _elf_magic_bytes {
		"ei_mag[\(key)]": #binary_component & {
			description: "The magic byte defined for ELF[\( key )]"
			length:      1
			value:       magic_value
			offset:      0 + key
			name:        "ei_mag[\(key)]"
		}
	}
}
_ei_class: #binary_component & {
	description: "Specifies the architecture of the binary"
	length:      1
	options: arch_multiplier: activate_on_value: _elf_class.elf64
	constraint: _elf_class
	name:       "ei_class"
}
_ei_data: #binary_component & {
	description: "Specifies the endianness of the binary"
	length:      1
	constraint:  _elf_data
	options: endianness: value_if_little: _elf_data.lsb
	name: "ei_data"
}
_ei_version: #binary_component & {
	description: "Specifies the version of the binary"
	length:      1
	value:       1
	name:        "ei_version"
}
_ei_osabi: #binary_component & {
	description: "Specifies the OS ABI of the binary"
	length:      1
	constraint:  _elf_osabi
	name:        "ei_osabi"
}
_ei_abiversion: #binary_component & {
	description: "Specifies the ABI version of the binary"
	length:      1
	value:       0
	name:        "ei_abiversion"
}
_ei_pad: #binary_component & {
	description: "Padding bytes"
	length:      _ei_nident.value - len(_ei_magic) - _ei_class.length - _ei_data.length - _ei_version.length - _ei_osabi.length - _ei_abiversion.length - _ei_nident.length
	name:        "ei_pad"
}
_ei_nident: #binary_component & {
	description: "Specifies the ABI version of the binary"
	length:      1
	value:       16
	name:        "ei_nident"
}

// ========================================
// Remaining Header
// ========================================
_elf_type: {
	none: 0
	rel:  1
	exec: 2
	dyn:  3
	core: 4
}
_e_type: {
	description: "Specifies the type of the binary."
	length:      _size.uint16
	value:       _elf_type
	name:        "e_type"
}
_elf_machine: {
	EM_NONE:        0
	EM_M32:         0
	EM_SPARC:       0
	EM_386:         0
	EM_68K:         0
	EM_88K:         0
	EM_860:         0
	EM_MIPS:        0
	EM_PARISC:      0
	EM_SPARC32PLUS: 0
	EM_PPC:         0
	EM_PPC64:       0
	EM_S390:        0
	EM_ARM:         0
	EM_SH:          0
	EM_SPARCV9:     0
	EM_IA_64:       0
	EM_X86_64:      0
	EM_VAX:         0
}
_e_machine: {
	description: "Specifies the machine type of the binary."
	length:      _size.uint16
	value:       _elf_machine
	name:        "e_machine"
}
_e_version: {
	description: "Specifies the version of the binary"
	length:      _size.uint32
	value:       1
	name:        "e_version"
}
_e_entry: _elf_off & {
	description: "Specifies the entry point of the binary."
	constraint:  "less than binary size"
	name:        "e_entry"
}
_e_phoff: _elf_off & {
	description: "Specifies the offset in the file of the program headers."
	constraint:  "less than binary size"
	name:        "e_phoff"
}
_e_shoff: _elf_off & {
	description: "Specifies the offset in the file of the section headers"
	constraint:  "less than binary size"
	name:        "e_shoff"
}
_e_flags: {
	description: "Specifies flags for the binary"
	length:      _size.uint32
	name:        "e_flags"
}
_e_ehsize: {
	description: "Specifies the size of the ELF header"
	length:      _size.uint16
	constraint: {elf32: 48, elf64: 64}
	name: "e_ehsize"
}
_e_phentsize: {
	description: "Specifies the size of each program header."
	length:      _size.uint16
	value: {elf32: 32, elf64: 56}
	name: "e_phentsize"
}
_e_phnum: {
	description: "Specifies the number of program headers present."
	length:      _size.uint16
	constraint:  "less than binary size"
	name:        "e_phnum"
}
_e_shentsize: {
	description: "Specifies the size of each section header."
	length:      _size.uint16
	value: {elf32: 40, elf64: 64}
	name: "e_shentsize"
}
_e_shnum: {
	description: "Specifies the number of section headers."
	length:      _size.uint16
	constraint:  "less than binary size"
	name:        "e_shnum"
}
_e_shstrndx: {
	description: "Specifies the index in the section headers of the string table."
	length:      _size.uint16
	constraint: {}
	name: "e_shstrndx"
}

// ========================================
// Program Header
// ========================================
elf_program_header: #binary_component & {
	name:         "elf_program_header"
	description:  "Describes the program headers for the binary."
	offset:       "elf_header.e_phoff"
	repeat_count: "elf_header.e_phnum"
	generates: [
		_p_type,
		_p_flags_64,
		_p_offset,
		_p_vaddr,
		_p_paddr,
		_p_filesz,
		_p_memsz,
		_p_flags_32,
		_p_align,
	]
}
_p_type: {
	name:        "p_type"
	description: "Describes the type of this header."
	length:      _size.uint32
	value: {
		null:      0
		load:      0
		dynamic:   0
		interp:    0
		note:      0
		shlib:     0
		phdr:      0
		loproc:    0
		hiproc:    0
		gnu_stack: 0
	}
}
_p_flags_bits: {
	PF_X: 0b001
	PF_W: 0b010
	PF_R: 0b100
}
_p_flags_64: _elf_skip_if_32 & {
	name:        "p_flags"
	description: "Specifies flags for this header when loaded"
	length:      _size.uint32
	options: bitmask_values: _p_flags_bits
}
_p_flags_32: _elf_skip_if_64 & {
	name:        "p_flags"
	description: "Specifies flags for this header when loaded"
	length:      _size.uint32
	options: bitmask_values: _p_flags_bits
}
_p_offset: _elf_off & {
	name:        "p_offset"
	description: "Specifies the offset in the file of this code section."
	length:      _size.uint32
}
_p_vaddr: _elf_addr & {
	name:        "p_vaddr"
	description: "Specifies the virtual address at which this segment should be loaded in memory."
	length:      _size.uint32
}
_p_paddr: _elf_addr & {
	name:        "p_paddr"
	description: "Specifies the physical address at which this segment should be loaded in memory."
	length:      _size.uint32
}
_p_filesz: _elf_off & {
	name:        "p_filesz"
	description: "Specifies the length of the segment in the file."
	length:      _size.uint32
}
_p_memsz: _elf_off & {
	name:        "p_memsz"
	description: "Specifies the length of the segment when loaded into memory."
	length:      _size.uint32
}
_p_align: _elf_off & {
	name:        "p_align"
	description: "Specifies the alignment of the segment in memory."
	length:      _size.uint32
}

// ========================================
// Section Header
// ========================================
_elf_section_types: {
	NULL:     0
	PROGBITS: 0
	SYMTAB:   0
	STRTAB:   0
	RELA:     0
	REL:      0
	HASH:     0
	DYNAMIC:  0
	NOTE:     0
	NOBITS:   0
	SHLIB:    0
	DYNSYM:   0
	LOPROC:   0
	HIPROC:   0
	LOUSER:   0
	HIUSER:   0
}

elf_section_header: #binary_component & {
	name:         "elf_section_header"
	description:  "Specifies information for the compile-time loader to understand linking layouts."
	offset:       "elf_header.e_shoff"
	repeat_count: "elf_header.e_shnum"
	generates: [
		_sh_name,
		_sh_type,
		_sh_flags,
		_sh_addr,
		_sh_offset,
		_sh_size,
		_sh_link,
		_sh_info,
		_sh_addralign,
		_sh_entsize,
	]
	options: points_to: [
		_elf_symbol_table,
	]
}
_sh_name: {
	name:        "sh_name"
	description: "Specifies the offset in the strndx table for the start of the name."
	length:      _size.uint32
}

_sh_type: {
	name:        "sh_type"
	description: "Specifies the type of this section."
	value:       _elf_section_types
	length:      _size.uint32
}

_sh_flags: _elf_off & {
	name:        "sh_flags"
	description: "Specifies the flags for this section."
	length:      _size.uint32
}

_sh_addr: _elf_off & {
	name:        "sh_addr"
	description: "Specifies the address in memory where this section should be loaded."
	length:      _size.uint32
}

_sh_offset: _elf_off & {
	name:        "sh_offset"
	description: "Specifies the offset in the file of this section."
	length:      _size.uint32
}

_sh_size: _elf_off & {
	name:        "sh_size"
	description: "Specifies the size in the file of this section."
	length:      _size.uint32
}

_sh_link: {
	name:        "sh_link"
	description: "Specifies the linked section. This is often used for eg. string tables."
	length:      _size.uint32
}

_sh_info: {
	name:        "sh_info"
	description: "Specifies additional information for the section."
	length:      _size.uint32
}

_sh_addralign: _elf_off & {
	name:        "sh_addralign"
	description: "Specifies the virtual memory alignment."
	length:      _size.uint32
}

_sh_entsize: _elf_off & {
	name:        "sh_entsize"
	description: "Specifies the entrysize of each entry in the section (such as for symbol tables)."
	length:      _size.uint32
}

// ========================================
// Symbol Table
// ========================================
_elf_symbol_table: {
	name:        "symbol_table"
	description: "The table of symbols exposed."
	when:        "self.sh_type == _elf_section_types.SYMTAB"
	generates: [
		_e_symbol_32,
		_e_symbol_64,
	]
}
_e_symbol_32: _elf_skip_if_64 & {
	name:        "symbol_table_entry"
	description: "Entry in the symbol table."
	generates: [
		_st_name,
		_st_value,
		_st_size,
		_st_info,
		_st_other,
		_st_shndx,
	]
}
_e_symbol_64: _elf_skip_if_32 & {
	name:        "symbol_table_entry"
	description: "Entry in the symbol table."
	generates: [
		_st_name,
		_st_info,
		_st_other,
		_st_shndx,
		_st_value,
		_st_size,
	]
}
_st_name: {
	name:        "st_name"
	description: "Offset into the linked symbol table string table for this symbol name."
	length:      _size.uint32
}
_st_info: {
	name:        "st_info"
	description: "Additional information for the symbol."
	length:      _size.uint8
}
_st_other: {
	name:        "st_other"
	description: "Additional information for the symbol."
	length:      _size.uint8
}
_st_shndx: {
	name:        "st_shndx"
	description: "Index in the section headers to the linked section for this symbol."
	length:      _size.uint16
}
_st_value: _elf_addr & {
	name:        "st_value"
	description: "Value stored by the symbol."
	length:      _size.uint32
}
_st_size: _elf_addr & {
	name:        "st_size"
	description: "Size in bytes of this symbol"
	length:      _size.uint32
}
