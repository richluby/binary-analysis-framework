package baf

_regex_path: "[.-a-zA-Z0-9]"

#option: {
	// Endianness option will determine the endianness 
	// for the entirety of the parsed binary
	endianness?: {
		value_if_little: int
	}

	// Architecture multiplier enables switching on a value
	// in the binary. The multiplier will be applied to size constraints
	// to enable 32/64 bit support in a single specification.
	arch_multiplier?: {
		activate_on_value: _
		multiplier:        *2 | uint
	}
	active_arch_multiplier?: *false | true

	// length_in_bits will cause the length
	// field to be interpreted as a series of
	// bits according to the file endianness
	length_in_bits?: *false | true

	// Enable skipping fields given certain conditions. This
	// provides architecture-specific layouts.
	skip_if?: string

	// Enable decoding bitmask fields
	bitmask_values?: {
		[string]: uint
	}

	// Enable decoding additional information based on a condition
	points_to?: [#pointer_definition, ...#pointer_definition]
}

#metadata: {
	description: !=""
	name:        !=""
}

// Indicates when a binary component points to decoding additional constructs sometimes
#pointer_definition: {
	when: string
	#binary_component
}

// The atomic type for the specification.
#binary_component: {
	#metadata
	options?: #option
	offset?:  uint | =~_regex_path
	// Ensure the value in the file
	// falls within the specified constraints.
	value?: _
	// Enable CUE-style constraints on the value
	constraint?: string | {[string]: _}
	#length_specifiers
}

#length_specifiers: {
	length: int & >0
} | {
	generates: [{#binary_component}, ...{#binary_component}]
	// Continue to generate the defined structure until a specified
	// value. Example: NUL-terminated strings
	until?: _

	// repeat_count determines how many of this binary component
	// to repeat in a contiguous run. if a string, it will be
	// interpreted as a path to an element from which to fetch
	// the repeat count.
	repeat_count?: uint | =~_regex_path
}

_size: {
	uint8:   1
	uint16:  2
	uint32:  4
	uint64:  8
	uint128: 16
}
